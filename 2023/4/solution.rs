use std::collections::HashMap;
use std::collections::HashSet;
use std::convert::TryFrom;
use std::convert::TryInto;
use std::fs;

struct Card {
    id: usize,
    winning_numbers: HashSet<u8>,
    my_numbers: HashSet<u8>,
}

struct Cards {
    data: Vec<Card>,
}

impl Cards {
    fn from_str(lines: &str) -> Cards {
        Cards {
            data: lines
                .split("\n")
                .enumerate()
                .map(|(i, l)| Card::from_str(i, l))
                .collect(),
        }
    }

    fn compute_points(&self) -> u32 {
        self.data.iter().map(|c| c.compute_points()).sum::<u32>()
    }

    fn count_bonus_cards(&self) -> u32 {
        let mut nb_cards_by_id: HashMap<usize, u32> =
            self.data.iter().map(|c| (c.id, 1u32)).collect();

        for card in self.data.iter() {
            let nb_wins = self.data[card.id].nb_matching_numbers();
            let current_nb_cards = *nb_cards_by_id.get(&card.id).unwrap();
            for i in card.id + 1..=card.id + usize::try_from(nb_wins).unwrap() {
                *nb_cards_by_id.get_mut(&i).unwrap() += current_nb_cards;
            }
        }

        nb_cards_by_id.values().map(|v| *v).sum()
    }
}

impl Card {
    fn from_str(id: usize, value: &str) -> Card {
        let mut numbers = value.split(": ").nth(1).unwrap().split(" | ");

        Card {
            id,
            winning_numbers: numbers
                .next()
                .unwrap()
                .split(" ")
                .map(|n| n.trim())
                .filter(|n| n.len() > 0)
                .map(|n| n.parse().unwrap())
                .collect(),
            my_numbers: numbers
                .next()
                .unwrap()
                .split(" ")
                .map(|n| n.trim())
                .filter(|n| n.len() > 0)
                .map(|n| n.parse().unwrap())
                .collect(),
        }
    }

    fn nb_matching_numbers(&self) -> u32 {
        self.winning_numbers
            .intersection(&self.my_numbers)
            .count()
            .try_into()
            .unwrap()
    }

    fn compute_points(&self) -> u32 {
        let nb_winning_numbers = self.nb_matching_numbers();
        if nb_winning_numbers == 0 {
            return 0;
        }

        2u32.pow(nb_winning_numbers - 1)
    }
}

fn test() {
    println!("[Day 4] Tests");
    let cards = Cards::from_str(
        &[
            "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
            "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
            "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
            "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
            "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
            "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
        ]
        .join("\n"),
    );
    println!("{}", cards.data[0].compute_points() == 8);
    println!("{}", cards.data[2].compute_points() == 2);
    println!("{}", cards.data[3].compute_points() == 1);
    println!("{}", cards.data[5].compute_points() == 0);
    println!("{}", cards.compute_points() == 13);

    println!("{}", cards.count_bonus_cards() == 30);

    println!("");
}

fn read_file(path: &str) -> String {
    fs::read_to_string(path).expect("Should be able to load file")
}

fn solve() {
    println!("[Day 4] Solving");

    let cards = Cards::from_str(&read_file("input.txt"));
    println!("Part 1: {}", cards.compute_points());
    println!("Part 2: {}", cards.count_bonus_cards());
}

fn main() {
    test();

    solve();
}
