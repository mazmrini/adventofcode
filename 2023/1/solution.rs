use std::fs;

struct Calibrations {
    data: Vec<String>,
}

impl Calibrations {
    fn from_str(value: &str) -> Calibrations {
        Calibrations {
            data: value
                .split("\n")
                .map(|el| el.to_string())
                .collect::<Vec<String>>(),
        }
    }

    fn convert(value: String) -> String {
        let numbers = vec![
            "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
        ];
        let lowest_index = numbers
            .iter()
            .enumerate()
            .map(|(i, n)| (i, value.find(n)))
            .filter(|(_, n)| n.is_some())
            .map(|(i, n)| (i, n.unwrap()))
            .min_by(|(_, a), (_, b)| a.cmp(b))
            .map(|(i, _)| i);

        return match lowest_index {
            Some(n) => Self::convert(value.replacen(numbers[n], &format!("{}", n + 1), 1)),
            None => value,
        };
    }

    fn calibrate(&self) -> u32 {
        let numbers: Vec<(&str, &str)> = vec![
            ("1", "1"),
            ("2", "2"),
            ("3", "3"),
            ("4", "4"),
            ("5", "5"),
            ("6", "6"),
            ("7", "7"),
            ("8", "8"),
            ("9", "9"),
        ];

        self.data
            .iter()
            .map(|word| self.decypher(word, &numbers))
            .sum()
    }

    fn calibrate_part_2(&self) -> u32 {
        let numbers: Vec<(&str, &str)> = vec![
            ("1", "1"),
            ("2", "2"),
            ("3", "3"),
            ("4", "4"),
            ("5", "5"),
            ("6", "6"),
            ("7", "7"),
            ("8", "8"),
            ("9", "9"),
            ("one", "1"),
            ("two", "2"),
            ("three", "3"),
            ("four", "4"),
            ("five", "5"),
            ("six", "6"),
            ("seven", "7"),
            ("eight", "8"),
            ("nine", "9"),
        ];

        self.data
            .iter()
            .map(|word| self.decypher(word, &numbers))
            .sum()
    }

    fn decypher(&self, word: &str, numbers: &Vec<(&str, &str)>) -> u32 {
        let first_found: &str = numbers
            .iter()
            .map(|(to_find, value)| (word.find(to_find), value))
            .filter(|(opt, value)| opt.is_some())
            .map(|(opt, value)| (opt.unwrap(), value))
            .min_by(|(a, _), (b, _)| a.cmp(b))
            .map(|(opt, value)| value)
            .unwrap();

        let last_found: &str = numbers
            .iter()
            .map(|(to_find, value)| (word.rfind(to_find), value))
            .filter(|(opt, value)| opt.is_some())
            .map(|(opt, value)| (opt.unwrap(), value))
            .max_by(|(a, _), (b, _)| a.cmp(b))
            .map(|(opt, value)| value)
            .unwrap();

        (first_found.to_string() + last_found).parse().unwrap()
    }
}

fn test() {
    println!("[Day 1] Tests");

    let mut calibrations = Calibrations::from_str("1abc2\npqr3stu8vwx\na1b2c3d4e5f\ntreb7uchet");
    let expected = 12 + 38 + 15 + 77;
    println!("{}", calibrations.calibrate() == expected);

    calibrations = Calibrations::from_str(
        "two1nine\neightwothree\nabcone2threexyz\nxtwone3four\n4nineeightseven2\nzoneight234\n7pqrstsixteen",
    );
    let expected = 29 + 83 + 13 + 24 + 42 + 14 + 76;
    println!("{}", calibrations.calibrate_part_2() == expected);
}

fn read_file(path: &str) -> String {
    fs::read_to_string(path).expect("Should be able to load file")
}

fn solve() {
    println!("[Day 1] Solving");

    let mut calibrations = Calibrations::from_str(&read_file("input.txt"));
    println!("Part 1: {}", calibrations.calibrate());
    println!("Part 2: {}", calibrations.calibrate_part_2());
}

fn main() {
    test();

    solve();
}
