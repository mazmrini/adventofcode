use std::collections::HashMap;
use std::fs;

struct Room([String; 2]);

struct Network {
    instructions: Vec<usize>,
    rooms: HashMap<String, Room>,
}

impl Room {
    fn from_str(value: &str) -> Room {
        Room([value[1..4].to_string(), value[6..9].to_string()])
    }
}

impl Network {
    fn from_str(value: &str) -> Network {
        let instructions_map: HashMap<char, usize> = HashMap::from([('L', 0), ('R', 1)]);
        let mut rooms: HashMap<String, Room> = HashMap::new();
        for room_str in value.split("\n\n").nth(1).unwrap().split("\n") {
            let mut splits = room_str.split(" = ");
            let room = splits.next().unwrap().to_string();
            rooms.insert(room, Room::from_str(splits.next().unwrap()));
        }

        Network {
            instructions: value
                .split("\n\n")
                .next()
                .unwrap()
                .chars()
                .map(|c| instructions_map[&c])
                .collect(),
            rooms,
        }
    }

    fn find_route(&self, from: &str, should_stop: &dyn Fn(&String) -> bool) -> Vec<String> {
        let mut current = from.to_string();
        let mut route: Vec<String> = vec![];
        let mut i: usize = 0;
        while !should_stop(&current) {
            current =
                self.rooms[&current].0[self.instructions[i % self.instructions.len()]].to_string();
            route.push(current.to_string());

            i += 1;
        }

        route
    }

    fn find_ghost_route_len(&self) -> usize {
        let ends_with_z = |v: &String| v.ends_with('Z');
        let closest_zs: Vec<usize> = self
            .rooms
            .keys()
            .filter(|r| r.ends_with('A'))
            .map(|k| self.find_route(k, &ends_with_z).len())
            .collect();
        let farthest_z: usize = *closest_zs.iter().max().unwrap();
        let mut lcm_candidate = 0;
        let mut max_modulo: usize = 1;
        while max_modulo != 0 {
            lcm_candidate += farthest_z;
            max_modulo = closest_zs.iter().map(|z| lcm_candidate % z).max().unwrap();
        }

        lcm_candidate
    }
}

fn test() {
    println!("[Day 8] Tests");
    println!("[Part 1] Tests");
    let network = Network::from_str(
        &("RL\n\n".to_string()
            + "AAA = (BBB, CCC)\n"
            + "BBB = (DDD, EEE)\n"
            + "CCC = (ZZZ, GGG)\n"
            + "DDD = (DDD, DDD)\n"
            + "EEE = (EEE, EEE)\n"
            + "GGG = (GGG, GGG)\n"
            + "ZZZ = (ZZZ, ZZZ)"),
    );
    println!("{}", network.find_route(&"AAA", &|v| v == "ZZZ").len() == 2);

    let network = Network::from_str(
        &("LLR\n\n".to_string() + "AAA = (BBB, BBB)\n" + "BBB = (AAA, ZZZ)\n" + "ZZZ = (ZZZ, ZZZ)"),
    );
    println!(
        "{}",
        network.find_route(&"AAA", &|v| v == "ZZZ")
            == vec!["BBB", "AAA", "BBB", "AAA", "BBB", "ZZZ"]
    );

    println!("[Part 2] Tests");
    let network = Network::from_str(
        &("LR\n\n".to_string()
            + "11A = (11B, XXX)\n"
            + "11B = (XXX, 11Z)\n"
            + "11Z = (11B, XXX)\n"
            + "22A = (22B, XXX)\n"
            + "22B = (22C, 22C)\n"
            + "22C = (22Z, 22Z)\n"
            + "22Z = (22B, 22B)\n"
            + "XXX = (XXX, XXX)"),
    );
    println!("{}", network.find_ghost_route_len() == 6);

    println!("");
}

fn read_file(path: &str) -> String {
    fs::read_to_string(path).expect("Should be able to load file")
}

fn solve() {
    println!("[Day 8] Solving");

    let network = Network::from_str(&read_file("input.txt"));
    println!(
        "Part 1: {}",
        network.find_route(&"AAA", &|v| v == "ZZZ").len()
    );
    println!("Part 2: {}", network.find_ghost_route_len());
}

fn main() {
    test();

    solve();
}
