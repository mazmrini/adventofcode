use std::collections::HashMap;
use std::fs;
use std::iter::FromIterator;

struct TransitionMap {
    from: u64,
    to: u64,
    range: u64,
}

struct Transitions {
    data: Vec<TransitionMap>,
}

struct PlantGuide {
    seeds: Vec<u64>,
    steps: Vec<String>,
    steps_map: HashMap<String, Transitions>,
}

impl TransitionMap {
    fn from_str(value: &str) -> TransitionMap {
        let values: Vec<u64> = value.split(" ").map(|v| v.parse().unwrap()).collect();

        TransitionMap {
            from: values[1],
            to: values[0],
            range: values[2],
        }
    }

    fn transition(&self, value: u64) -> Option<u64> {
        self._transition(value, self.from, self.to)
    }

    fn rev_transition(&self, value: u64) -> Option<u64> {
        self._transition(value, self.to, self.from)
    }

    fn _transition(&self, value: u64, from: u64, to: u64) -> Option<u64> {
        if from <= value && value < from + self.range {
            let diff = value - from;
            return Some(to + diff);
        }

        None
    }
}

impl Transitions {
    fn from_str(value: &str) -> Transitions {
        let mut lines = value.split("\n");
        lines.next();

        Transitions {
            data: lines.map(|l| TransitionMap::from_str(l)).collect(),
        }
    }

    fn rev_transition(&self, value: u64) -> u64 {
        for transition in self.data.iter() {
            if let Some(transformed) = transition.rev_transition(value) {
                return transformed;
            }
        }

        value
    }

    fn transition(&self, value: u64) -> u64 {
        for transition in self.data.iter() {
            if let Some(transformed) = transition.transition(value) {
                return transformed;
            }
        }

        value
    }
}

impl PlantGuide {
    fn from_str(value: &str) -> PlantGuide {
        let mut paragraphs = value.split("\n\n");
        let seeds: Vec<u64> = paragraphs
            .next()
            .unwrap()
            .split("seeds: ")
            .last()
            .unwrap()
            .split(" ")
            .map(|c| c.parse().unwrap())
            .collect();
        let steps: Vec<&str> = vec![
            "soil",
            "fertilizer",
            "water",
            "light",
            "temperature",
            "humidity",
            "location",
        ];

        PlantGuide {
            seeds,
            steps: steps.iter().map(|c| c.to_string()).collect(),
            steps_map: HashMap::from_iter(steps.iter().map(|s| {
                (
                    s.to_string(),
                    Transitions::from_str(paragraphs.next().unwrap()),
                )
            })),
        }
    }

    fn part_1(&self) -> u64 {
        self.seeds
            .iter()
            .map(|s| self.find_location(s))
            .min()
            .unwrap()
    }

    fn part_2(&self) -> u64 {
        let ranges: Vec<(u64, u64)> = (0..self.seeds.len())
            .step_by(2)
            .map(|i| (self.seeds[i], self.seeds[i] + self.seeds[i + 1]))
            .collect();

        let mut lowest: u64 = 0;
        loop {
            let seed = self.find_seed_from_location(lowest);
            if self.is_in_range(&seed, &ranges) {
                return lowest;
            }

            lowest += 1;
        }
    }

    fn is_in_range(&self, value: &u64, ranges: &Vec<(u64, u64)>) -> bool {
        for r in ranges.iter() {
            if r.0 <= *value && *value < r.1 {
                return true;
            }
        }

        false
    }

    fn find_location(&self, seed: &u64) -> u64 {
        let mut current: u64 = *seed;
        for step in self.steps.iter() {
            current = self.steps_map.get(step).unwrap().transition(current);
        }

        current
    }

    fn find_seed_from_location(&self, value: u64) -> u64 {
        let mut current: u64 = value;
        for step in self.steps.iter().rev() {
            current = self.steps_map.get(step).unwrap().rev_transition(current);
        }

        current
    }
}

fn test() {
    println!("[Day 5] Tests");

    let plant_guide = PlantGuide::from_str(&read_file("test.txt"));
    println!("{}", plant_guide.seeds == vec![79, 14, 55, 13]);
    println!("{}", plant_guide.steps_map.len() == 7);
    println!(
        "{}",
        plant_guide.steps_map.get("temperature").unwrap().data.len() == 3
    );
    println!(
        "{}",
        plant_guide.steps_map.get("water").unwrap().data.len() == 4
    );
    println!("{}", plant_guide.part_1() == 35);
    println!("{}", plant_guide.part_2() == 46);

    println!("");
}

fn read_file(path: &str) -> String {
    fs::read_to_string(path).expect("Should be able to load file")
}

fn solve() {
    println!("[Day 5] Solving");

    // Return to previous commit to run this bit
    let plant_guide = PlantGuide::from_str(&read_file("input.txt"));
    println!("Part 1: {}", plant_guide.part_1());
    println!("Part 2: {}", plant_guide.part_2());
}

fn main() {
    test();

    solve();
}
