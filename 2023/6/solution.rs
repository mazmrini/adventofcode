use std::fs;

fn read_file(path: &str) -> String {
    fs::read_to_string(path).expect("Should be able to load file")
}

fn solve() {
    println!("[Day 8] Solving");

    println!("[Part 1]\n{}\n", read_file("part_1.txt"));
    println!("[Part 2]\n{}", read_file("part_2.txt"));
}

fn main() {
    solve();
}
