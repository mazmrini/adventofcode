use std::collections::HashMap;
use std::fs;

struct Report {
    value: Vec<i32>,
    hash: String,
}

struct Oasis {
    history: Vec<Report>,
}

impl Report {
    fn from_str(value: &str) -> Report {
        Report {
            value: value.split(" ").map(|c| c.parse().unwrap()).collect(),
            hash: value.to_string(),
        }
    }

    fn predict_next(&self, nexts: &mut HashMap<String, i32>) -> i32 {
        if self.value.iter().filter(|v| **v != 0).count() == 0 {
            nexts.insert(self.hash.to_string(), 0);
            return 0;
        }

        let sub_value: Vec<i32> = (0..self.value.len() - 1)
            .map(|i| self.value[i + 1] - self.value[i])
            .collect();
        let sub_hash = sub_value
            .iter()
            .map(|i| i.to_string())
            .collect::<Vec<String>>()
            .join(" ")
            .to_string();
        let sub_report = Report {
            value: sub_value,
            hash: sub_hash,
        };
        let sub_report_prediction = sub_report.predict_next(nexts);
        let self_prediction = self.value.last().unwrap() + sub_report_prediction;
        nexts.insert(self.hash.to_string(), self_prediction);

        self_prediction
    }

    fn predict_previous(&self, prevs: &mut HashMap<String, i32>) -> i32 {
        if self.value.iter().filter(|v| **v != 0).count() == 0 {
            prevs.insert(self.hash.to_string(), 0);
            return 0;
        }

        let sub_value: Vec<i32> = (0..self.value.len() - 1)
            .map(|i| self.value[i + 1] - self.value[i])
            .collect();
        let sub_hash = sub_value
            .iter()
            .map(|i| i.to_string())
            .collect::<Vec<String>>()
            .join(" ")
            .to_string();
        let sub_report = Report {
            value: sub_value,
            hash: sub_hash,
        };
        let sub_report_prediction = sub_report.predict_previous(prevs);
        let self_prediction = self.value.first().unwrap() - sub_report_prediction;
        prevs.insert(self.hash.to_string(), self_prediction);

        self_prediction
    }
}

impl Oasis {
    fn from_str(value: &str) -> Oasis {
        Oasis {
            history: value
                .split("\n")
                .map(|line| Report::from_str(line))
                .collect(),
        }
    }

    fn next_prediction_sum(&self) -> i32 {
        let mut nexts: HashMap<String, i32> = HashMap::new();

        self.history
            .iter()
            .map(|c| c.predict_next(&mut nexts))
            .sum()
    }

    fn prev_prediction_sum(&self) -> i32 {
        let mut prevs: HashMap<String, i32> = HashMap::new();

        self.history
            .iter()
            .map(|c| c.predict_previous(&mut prevs))
            .sum()
    }
}

fn test() {
    println!("[Day 9] Tests");
    println!("[Part 1] Tests");

    let oasis =
        Oasis::from_str(&["0 3 6 9 12 15", "1 3 6 10 15 21", "10 13 16 21 30 45"].join("\n"));
    println!(
        "{}",
        oasis.history[0].predict_next(&mut HashMap::new()) == 18
    );
    println!(
        "{}",
        oasis.history[1].predict_next(&mut HashMap::new()) == 28
    );
    println!(
        "{}",
        oasis.history[2].predict_next(&mut HashMap::new()) == 68
    );
    println!("{}", oasis.next_prediction_sum() == 114);

    println!("[Part 2] Tests");
    let oasis =
        Oasis::from_str(&["0 3 6 9 12 15", "1 3 6 10 15 21", "10 13 16 21 30 45"].join("\n"));
    println!(
        "{}",
        oasis.history[0].predict_previous(&mut HashMap::new()) == -3
    );
    println!(
        "{}",
        oasis.history[1].predict_previous(&mut HashMap::new()) == 0
    );
    println!(
        "{}",
        oasis.history[2].predict_previous(&mut HashMap::new()) == 5
    );
    println!("{}", oasis.prev_prediction_sum() == 2);

    println!("");
}

fn read_file(path: &str) -> String {
    fs::read_to_string(path).expect("Should be able to load file")
}

fn solve() {
    println!("[Day 9] Solving");

    let oasis = Oasis::from_str(&read_file("input.txt"));
    println!("Part 1: {}", oasis.next_prediction_sum());
    println!("Part 2: {}", oasis.prev_prediction_sum());
}

fn main() {
    test();

    solve();
}
