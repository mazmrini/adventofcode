use std::cmp::Ordering;
use std::collections::HashMap;
use std::fs;

#[repr(u8)]
#[derive(PartialEq, Clone, Copy)]
enum HandType {
    FiveOfAKind = 6,
    FourOfAKind = 5,
    FullHouse = 4,
    ThreeOfAKind = 3,
    TwoPairs = 2,
    OnePair = 1,
    HighCard = 0,
}

struct Player {
    hand: Vec<u8>,
    hand_type: HandType,
    bet: u32,
}

struct Referee {
    players: Vec<Player>,
}

trait Rules {
    fn new() -> Self
    where
        Self: Sized;
    fn hash(&self, hand: &str) -> String;
    fn to_hand_values(&self, hand: &str) -> Vec<u8>;
}

struct StandardRules {
    card_values: HashMap<char, u8>,
}
struct JackAsJokerRules {
    card_values: HashMap<char, u8>,
}

impl Rules for StandardRules {
    fn new() -> StandardRules {
        StandardRules {
            card_values: HashMap::from([
                ('2', 2),
                ('3', 3),
                ('4', 4),
                ('5', 5),
                ('6', 6),
                ('7', 7),
                ('8', 8),
                ('9', 9),
                ('T', 10),
                ('J', 11),
                ('Q', 12),
                ('K', 13),
                ('A', 14),
            ]),
        }
    }

    fn hash(&self, hand: &str) -> String {
        let mut counter: HashMap<char, u8> = HashMap::new();
        for c in hand.chars() {
            counter.entry(c).and_modify(|v| *v += 1).or_insert(1);
        }
        let mut sorted = counter.values().collect::<Vec<&u8>>();
        sorted.sort();

        sorted
            .iter()
            .rev()
            .map(|c| c.to_string())
            .collect::<Vec<String>>()
            .join("-")
    }

    fn to_hand_values(&self, hand: &str) -> Vec<u8> {
        hand.chars()
            .map(|c| *self.card_values.get(&c).unwrap())
            .collect()
    }
}

impl Rules for JackAsJokerRules {
    fn new() -> JackAsJokerRules {
        let mut card_values = StandardRules::new().card_values;
        card_values.insert('J', 1); // jack is now a joker
        JackAsJokerRules { card_values }
    }

    fn hash(&self, value: &str) -> String {
        if value == "JJJJJ" {
            return "5".to_string();
        }

        let mut counter: HashMap<char, u8> = HashMap::new();
        for c in value.chars().filter(|c| c != &'J') {
            counter.entry(c).and_modify(|v| *v += 1).or_insert(1);
        }
        let mut sorted: Vec<u8> = counter.values().map(|c| *c).collect();
        sorted.sort();
        let total: u8 = sorted.iter().sum();
        if let Some(last) = sorted.last_mut() {
            *last += 5u8 - total;
        }

        sorted
            .iter()
            .rev()
            .map(|c| c.to_string())
            .collect::<Vec<String>>()
            .join("-")
    }

    fn to_hand_values(&self, hand: &str) -> Vec<u8> {
        hand.chars()
            .map(|c| *self.card_values.get(&c).unwrap())
            .collect()
    }
}

impl Player {
    fn from_str(value: &str, rules: &dyn Rules) -> Player {
        let mut split = value.split(" ");

        Player::new(
            split.next().unwrap(),
            split.next().unwrap().parse().unwrap(),
            rules,
        )
    }

    fn new(hand: &str, bet: u32, rules: &dyn Rules) -> Player {
        Player {
            hand: rules.to_hand_values(hand),
            hand_type: Self::find_hand_type(rules.hash(hand), hand),
            bet,
        }
    }

    fn cmp_hand(&self, other: &Player) -> Ordering {
        if (self.hand_type as u8) > (other.hand_type as u8) {
            return Ordering::Greater;
        }
        if (self.hand_type as u8) < (other.hand_type as u8) {
            return Ordering::Less;
        }

        for (i, self_card) in self.hand.iter().enumerate() {
            let other_card = other.hand.get(i).unwrap();
            if self_card > other_card {
                return Ordering::Greater;
            }
            if self_card < other_card {
                return Ordering::Less;
            }
        }

        Ordering::Equal
    }

    fn find_hand_type(hash: String, value: &str) -> HandType {
        match hash.as_str() {
            "5" => HandType::FiveOfAKind,
            "4-1" => HandType::FourOfAKind,
            "3-2" => HandType::FullHouse,
            "3-1-1" => HandType::ThreeOfAKind,
            "2-2-1" => HandType::TwoPairs,
            "2-1-1-1" => HandType::OnePair,
            "1-1-1-1-1" => HandType::HighCard,
            _ => panic!("Impossible to find hand type for '{}'", value),
        }
    }
}

impl Referee {
    fn from_str(value: &str, rules: &dyn Rules) -> Referee {
        Referee {
            players: value
                .split("\n")
                .map(|p| Player::from_str(p, rules))
                .collect(),
        }
    }

    fn compute_winnings(&mut self) -> u32 {
        self.players.sort_by(|a, b| a.cmp_hand(b));

        self.players
            .iter()
            .zip(0u32..)
            .map(|(p, i)| p.bet * (i + 1))
            .sum()
    }
}

fn test() {
    println!("[Day 7] Tests");
    println!("[Part 1] Tests");

    let rules = StandardRules::new();
    println!(
        "{}",
        Player::new("AAAAA", 1, &rules).hand_type == HandType::FiveOfAKind
    );
    println!(
        "{}",
        Player::new("AA8AA", 1, &rules).hand_type == HandType::FourOfAKind
    );
    println!(
        "{}",
        Player::new("23332", 1, &rules).hand_type == HandType::FullHouse
    );
    println!(
        "{}",
        Player::new("TTT98", 1, &rules).hand_type == HandType::ThreeOfAKind
    );
    println!(
        "{}",
        Player::new("23432", 1, &rules).hand_type == HandType::TwoPairs
    );
    println!(
        "{}",
        Player::new("A23A4", 1, &rules).hand_type == HandType::OnePair
    );
    println!(
        "{}",
        Player::new("23456", 1, &rules).hand_type == HandType::HighCard
    );

    let player_1 = Player::new("T55J5", 10, &rules);
    let player_2 = Player::new("QQQJA", 10, &rules);
    println!("{}", player_1.cmp_hand(&player_2) == Ordering::Less);
    println!("{}", player_1.cmp_hand(&player_1) == Ordering::Equal);
    println!("{}", player_2.cmp_hand(&player_1) == Ordering::Greater);
    let player_1 = Player::new("AAAAA", 10, &rules);
    let player_2 = Player::new("QQQJA", 10, &rules);
    println!("{}", player_1.cmp_hand(&player_2) == Ordering::Greater);
    println!(
        "{}",
        Player::new("TJ22J", 10, &rules).hand == vec![10, 11, 2, 2, 11]
    );

    let mut referee = Referee::from_str(
        &vec![
            "32T3K 765",
            "T55J5 684",
            "KK677 28",
            "KTJJT 220",
            "QQQJA 483",
        ]
        .join("\n"),
        &rules,
    );
    println!("{}", referee.compute_winnings() == 6440);

    println!("[Part 2] Tests");
    let rules = JackAsJokerRules::new();
    println!(
        "{}",
        Player::new("TJ22J", 10, &rules).hand == vec![10, 1, 2, 2, 1]
    );
    println!(
        "{}",
        Player::new("AAAAA", 1, &rules).hand_type == HandType::FiveOfAKind
    );
    println!(
        "{}",
        Player::new("JJJJJ", 1, &rules).hand_type == HandType::FiveOfAKind
    );
    println!(
        "{}",
        Player::new("J2JJJ", 1, &rules).hand_type == HandType::FiveOfAKind
    );
    println!(
        "{}",
        Player::new("JQJQ2", 1, &rules).hand_type == HandType::FourOfAKind
    );
    println!(
        "{}",
        Player::new("22J33", 1, &rules).hand_type == HandType::FullHouse
    );
    println!(
        "{}",
        Player::new("2345J", 1, &rules).hand_type == HandType::OnePair
    );
    println!(
        "{}",
        Player::new("23456", 1, &rules).hand_type == HandType::HighCard
    );

    let mut referee = Referee::from_str(
        &vec![
            "32T3K 765",
            "T55J5 684",
            "KK677 28",
            "KTJJT 220",
            "QQQJA 483",
        ]
        .join("\n"),
        &rules,
    );
    println!("{}", referee.compute_winnings() == 5905);

    println!("");
}

fn read_file(path: &str) -> String {
    fs::read_to_string(path).expect("Should be able to load file")
}

fn solve() {
    println!("[Day 7] Solving");

    let mut referee = Referee::from_str(&read_file("input.txt"), &StandardRules::new());
    println!("Part 1: {}", referee.compute_winnings());

    let mut referee = Referee::from_str(&read_file("input.txt"), &JackAsJokerRules::new());
    println!("Part 2: {}", referee.compute_winnings());
}

fn main() {
    test();

    solve();
}
