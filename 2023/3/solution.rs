use std::collections::HashSet;
use std::fs;

fn neighbours(locations: &HashSet<(usize, usize)>) -> HashSet<(usize, usize)> {
    let mut neighbours = HashSet::new();

    for loc in locations.iter() {
        neighbours.insert((loc.0, loc.1 - 1));
        neighbours.insert((loc.0, loc.1 + 1));
        neighbours.insert((loc.0 + 1, loc.1 - 1));
        neighbours.insert((loc.0 + 1, loc.1));
        neighbours.insert((loc.0 + 1, loc.1 + 1));
        neighbours.insert((loc.0 - 1, loc.1 - 1));
        neighbours.insert((loc.0 - 1, loc.1));
        neighbours.insert((loc.0 - 1, loc.1 + 1));
    }

    neighbours
}

struct Part {
    value: u32,
    locations: HashSet<(usize, usize)>,
}

struct Engine {
    parts: Vec<Part>,
    symbols_location: HashSet<(usize, usize)>,
    gears: HashSet<(usize, usize)>,
}

impl Part {
    fn is_neighbour(&self, other: &(usize, usize)) -> bool {
        !self
            .locations
            .is_disjoint(&neighbours(&HashSet::from([*other])))
    }
}

impl Engine {
    fn from_str(value: &str) -> Engine {
        let mut parts = Vec::new();
        let mut symbols_location = HashSet::new();
        let mut gears = HashSet::new();

        for (i, line) in value.split("\n").enumerate() {
            let mut part_str = String::new();
            let mut locations = Some(HashSet::new());
            for (j, char) in line.chars().enumerate() {
                if char.is_numeric() {
                    part_str.push(char);
                    locations
                        .get_or_insert(HashSet::new())
                        .insert((i + 1, j + 1));
                    continue;
                }

                if part_str.len() > 0 {
                    parts.push(Part {
                        value: part_str.parse().unwrap(),
                        locations: locations.take().unwrap(),
                    });
                    part_str = String::new();
                    locations = Some(HashSet::new());
                }

                if char != '.' {
                    symbols_location.insert((i + 1, j + 1));
                }

                if char == '*' {
                    gears.insert((i + 1, j + 1));
                }
            }

            if part_str.len() > 0 {
                parts.push(Part {
                    value: part_str.parse().unwrap(),
                    locations: locations.take().unwrap(),
                });
            }
        }

        Engine {
            parts,
            symbols_location,
            gears,
        }
    }

    fn sum_gears_ratios(&self) -> u32 {
        self.find_gears_ratios().iter().sum()
    }

    fn find_gears_ratios(&self) -> HashSet<u32> {
        self.gears
            .iter()
            .map(|g| {
                self.parts
                    .iter()
                    .filter(|p| p.is_neighbour(g))
                    .collect::<Vec<&Part>>()
            })
            .filter(|parts| parts.len() == 2)
            .map(|parts| parts[0].value * parts[1].value)
            .collect()
    }

    fn sum_parts(&self) -> u32 {
        self.find_parts().iter().map(|p| p.value).sum()
    }

    fn find_parts(&self) -> Vec<&Part> {
        self.parts
            .iter()
            .filter(|p| !self.symbols_location.is_disjoint(&neighbours(&p.locations)))
            .collect()
    }
}

fn test() {
    println!("[Day 3] Tests");

    let txt = vec![
        "467..114..",
        "...*......",
        "..35..633.",
        "......#...",
        "617*......",
        ".....+.58.",
        "..592.....",
        "......755.",
        "...$.*....",
        ".664.598..",
    ]
    .join("\n");
    let engine = Engine::from_str(&txt);
    println!("{}", engine.parts[1].value == 114);
    println!("{}", engine.parts[1].locations.contains(&(1, 6)));
    println!("{}", engine.parts[1].locations.contains(&(1, 7)));
    println!("{}", engine.parts[1].locations.contains(&(1, 8)));
    println!("{}", engine.parts[7].value == 755);
    println!("{}", engine.parts[7].locations.contains(&(8, 7)));
    println!("{}", engine.parts[7].locations.contains(&(8, 8)));
    println!("{}", engine.parts[7].locations.contains(&(8, 9)));
    println!("{}", engine.symbols_location.contains(&(2, 4)));
    println!("{}", engine.symbols_location.contains(&(9, 4)));
    println!("{}", engine.symbols_location.contains(&(9, 6)));
    println!("{}", engine.sum_parts() == 4361);

    let ratios = engine.find_gears_ratios();
    println!("{}", ratios.contains(&16345));
    println!("{}", ratios.contains(&451490));
    println!("{}", ratios.len() == 2);
    println!("{}", engine.sum_gears_ratios() == 467835);

    println!("");
}

fn read_file(path: &str) -> String {
    fs::read_to_string(path).expect("Should be able to load file")
}

fn solve() {
    println!("[Day 3] Solving");

    let engine = Engine::from_str(&read_file("input.txt"));
    println!("Part 1: {}", engine.sum_parts());
    println!("Part 2: {}", engine.sum_gears_ratios());
}

fn main() {
    test();

    solve();
}
