use std::cmp;
use std::fs;

struct GameSet {
    red: u32,
    blue: u32,
    green: u32,
}

impl GameSet {
    fn from_str(value: &str) -> GameSet {
        let mut set = GameSet {
            red: 0,
            blue: 0,
            green: 0,
        };
        for bit in value.split(", ") {
            let mut splits = bit.split(" ");
            let quantity: u32 = splits.next().unwrap().parse().unwrap();
            let color: &str = splits.next().unwrap();

            if color == "red" {
                set.red += quantity;
            } else if color == "blue" {
                set.blue += quantity;
            } else if color == "green" {
                set.green += quantity;
            }
        }

        return set;
    }

    fn is_in_bound(&self, bound: &GameSet) -> bool {
        self.red <= bound.red && self.green <= bound.green && self.blue <= bound.blue
    }

    fn fewest(&self, other: &GameSet) -> GameSet {
        GameSet {
            red: cmp::max(self.red, other.red),
            green: cmp::max(self.green, other.green),
            blue: cmp::max(self.blue, other.blue),
        }
    }

    fn power(&self) -> u32 {
        self.red * self.green * self.blue
    }
}

struct Game {
    id: u32,
    sets: Vec<GameSet>,
}

impl Game {
    fn from_lines(value: &str) -> Vec<Game> {
        value
            .split("\n")
            .map(|line| Game::from_line(line))
            .collect()
    }

    fn from_line(value: &str) -> Game {
        let mut splits = value.split(": ");
        let game_id: u32 = splits
            .next()
            .unwrap()
            .split(" ")
            .nth(1)
            .unwrap()
            .parse()
            .unwrap();

        Game {
            id: game_id,
            sets: splits
                .next()
                .unwrap()
                .split("; ")
                .map(|set| GameSet::from_str(set))
                .collect(),
        }
    }

    fn is_in_bound(&self, bound: &GameSet) -> bool {
        for set in self.sets.iter() {
            if !set.is_in_bound(bound) {
                return false;
            }
        }

        return true;
    }

    fn power(&self) -> u32 {
        let mut fewest = GameSet {
            red: 0,
            green: 0,
            blue: 0,
        };

        for set in self.sets.iter() {
            fewest = fewest.fewest(set);
        }

        fewest.power()
    }
}

fn solve_games(games: &Vec<Game>, bound: &GameSet) -> u32 {
    games
        .iter()
        .filter(|g| g.is_in_bound(bound))
        .map(|g| g.id)
        .sum()
}

fn test() {
    println!("[Day 2] Tests");
    let game_set_bound = GameSet {
        red: 12,
        green: 13,
        blue: 14,
    };

    let mut set_text = "3 blue, 4 red, 1 red, 2 green, 6 blue, 3 green, 1 blue";
    let mut game_set = GameSet::from_str(set_text);
    println!("{}", game_set.red == 5);
    println!("{}", game_set.blue == 10);
    println!("{}", game_set.green == 5);
    println!("{}", game_set.is_in_bound(&game_set_bound) == true);

    set_text = "3 blue, 4 red, 1 red, 2 green, 16 blue, 3 green, 1 blue";
    game_set = GameSet::from_str(set_text);
    println!("{}", game_set.red == 5);
    println!("{}", game_set.blue == 20);
    println!("{}", game_set.green == 5);
    println!("{}", game_set.is_in_bound(&game_set_bound) == false);

    let game_text: String = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n".to_owned()
        + "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\n"
        + "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\n"
        + "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\n"
        + "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";
    let games = Game::from_lines(&game_text);
    println!("{}", solve_games(&games, &game_set_bound) == 8);

    println!("{}", games[0].power() == 48);
    println!("{}", games[1].power() == 12);
    println!("{}", games[2].power() == 1560);
    println!("{}", games[3].power() == 630);
    println!("{}", games[4].power() == 36);

    println!("");
}

fn read_file(path: &str) -> String {
    fs::read_to_string(path).expect("Should be able to load file")
}

fn solve() {
    println!("[Day 2] Solving");
    let game_set_bound = GameSet {
        red: 12,
        green: 13,
        blue: 14,
    };

    let games = Game::from_lines(&read_file("input.txt"));
    println!("Part 1: {}", solve_games(&games, &game_set_bound));

    let total: u32 = games.iter().map(|gs| gs.power()).sum();
    println!("Part 2: {}", total);
}

fn main() {
    test();

    solve();
}
