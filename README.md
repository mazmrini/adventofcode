# adventofcode

## Setup 
This repo leverages [sbin](https://gitlab.com/mazmrini/bin)

```
pip install sbin
cd <year>

# make sure you have everything needed
bin req

# run a day
bin day <int>

# generate a day
bin gen <int>
```

## 2022

Rust
