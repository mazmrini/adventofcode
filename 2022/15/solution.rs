use std::collections::HashSet;
use std::fs;
use std::hash::Hash;

#[derive(Eq, PartialEq)]
struct Range(i64, i64);

impl Range {
    fn contains(&self, other: &i64) -> bool {
        other >= &self.0 && other < &self.1
    }
}

#[derive(Hash, Eq, PartialEq)]
struct Sensor {
    loc: (i64, i64),
    beacon: (i64, i64),
}

impl Sensor {
    pub fn from_str(value: &str) -> Sensor {
        let mut splits = value.split(": ");
        Sensor {
            loc: Sensor::from_sensor_str(splits.next().unwrap()),
            beacon: Sensor::from_beacon_str(splits.next().unwrap()),
        }
    }

    pub fn row_coverage(&self, row_nb: i64) -> Option<Range> {
        let col_coverage = self.manhattan_distance() - (self.loc.1 - row_nb).abs();
        if col_coverage < 0 {
            return None;
        }

        Some(Range(
            self.loc.0 - col_coverage,
            self.loc.0 + col_coverage + 1,
        ))
    }

    fn manhattan_distance(&self) -> i64 {
        (self.loc.0 - self.beacon.0).abs() + (self.loc.1 - self.beacon.1).abs()
    }

    fn from_sensor_str(value: &str) -> (i64, i64) {
        let mut splits = value.split(", ");
        let (_xtext, x) = splits.next().unwrap().split_at("Sensor at x=".len());
        let (_ytext, y) = splits.next().unwrap().split_at("y=".len());

        (x.parse().unwrap(), y.parse().unwrap())
    }

    fn from_beacon_str(value: &str) -> (i64, i64) {
        let mut splits = value.split(", ");
        let (_xtext, x) = splits
            .next()
            .unwrap()
            .split_at("closest beacon is at x=".len());
        let (_ytext, y) = splits.next().unwrap().split_at("y=".len());

        (x.parse().unwrap(), y.parse().unwrap())
    }
}

struct Sensors {
    pairs: HashSet<Sensor>,
}

impl Sensors {
    pub fn from_str(lines: &str) -> Sensors {
        let mut pairs = HashSet::new();
        for line in lines.split("\n") {
            pairs.insert(Sensor::from_str(line));
        }

        Sensors { pairs }
    }

    pub fn row_coverage(&self, row_nb: i64) -> Vec<Range> {
        let mut ranges: Vec<Range> = self
            .pairs
            .iter()
            .map(|p| p.row_coverage(row_nb))
            .filter(|r| !r.is_none())
            .map(|r| r.unwrap())
            .collect();
        ranges.sort_by(|a, b| a.0.cmp(&b.0));
        let mut i: usize = 0;
        while i < ranges.len() - 1 {
            let left = ranges.get(i).unwrap();
            let right = ranges.get(i + 1).unwrap();
            if right.1 <= left.1 {
                ranges.remove(i + 1);
            } else if right.0 <= left.1 {
                *ranges.get_mut(i).unwrap() = Range(left.0, right.1);
                ranges.remove(i + 1);
            } else {
                i += 1;
            }
        }

        ranges
    }

    pub fn nb_covered_locations(&self, row_nb: i64) -> i64 {
        let coverage = self.row_coverage(row_nb);
        let mut total_coverage: i64 = coverage.iter().map(|range| range.1 - range.0).sum();
        for b in self
            .pairs
            .iter()
            .map(|p| p.beacon)
            .collect::<HashSet<(i64, i64)>>()
            .iter()
            .filter(|b| b.1 == row_nb)
        {
            total_coverage -= coverage.iter().filter(|range| range.contains(&b.0)).count() as i64;
        }

        total_coverage
    }

    pub fn find_distress_beacon(&self, max_grid_size: i64) -> Option<(i64, i64)> {
        for y in 0..max_grid_size + 1 {
            let coverage = self.row_coverage(y);
            if coverage.len() > 1 {
                return Some((coverage.first().unwrap().1, y as i64));
            }
        }

        None
    }
}

fn test() {
    println!("[Day 15] Tests");
    let sensor = Sensor::from_str("Sensor at x=8, y=7: closest beacon is at x=2, y=10");
    println!("{}", sensor.row_coverage(-3) == None);
    println!("{}", sensor.row_coverage(18) == None);
    println!("{}", sensor.row_coverage(-2) == Some(Range(8, 9)));
    println!("{}", sensor.row_coverage(-1) == Some(Range(7, 10)));
    println!("{}", sensor.row_coverage(5) == Some(Range(1, 16)));
    println!("{}", sensor.row_coverage(7) == Some(Range(-1, 18)));
    println!("{}", sensor.row_coverage(12) == Some(Range(4, 13)));

    let sensors = read_file("test.txt");
    println!("[Tests] From test file");
    println!("{}", sensors.row_coverage(9) == vec![Range(-1, 24)]);
    println!("{}", sensors.row_coverage(10) == vec![Range(-2, 25)]);
    println!(
        "{}",
        sensors.row_coverage(11) == vec![Range(-3, 14), Range(15, 26)]
    );
    println!("{}", sensors.nb_covered_locations(10) == 26);
    println!("{}", sensors.find_distress_beacon(20) == Some((14, 11)));

    println!("");
}

fn read_file(path: &str) -> Sensors {
    Sensors::from_str(&fs::read_to_string(path).expect("Should be able to load file"))
}

fn solve() {
    println!("[Day 15] Solving");

    let sensors = read_file("input.txt");
    println!(
        "Covered locations @ y = 2_000_000: {}",
        sensors.nb_covered_locations(2000000)
    );

    let max_size: i64 = 4000000;
    let beacon = sensors.find_distress_beacon(max_size).unwrap();
    println!("Distress beacon found: ({}, {})", beacon.0, beacon.1);
    println!(
        "Distress beacon frequency: {}",
        beacon.0 * max_size + beacon.1
    );
}

fn main() {
    test();

    solve();
}
