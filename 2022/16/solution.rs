use std::collections::HashMap;
use std::collections::HashSet;
use std::fs;

#[derive(Eq, PartialEq)]
struct Valve {
    id: String,
    flow_rate: u32,
    neighbours: Vec<String>,
}

impl Valve {
    fn from_str(line: &str) -> Valve {
        let mut parts = line.split("; ");

        let valve_flow = parts.next().unwrap();
        let id = valve_flow
            .get("Valve ".len().."Valve XX".len())
            .unwrap()
            .to_string();
        let flow_rate: u32 = valve_flow
            .get("Valve XX has flow rate=".len()..)
            .unwrap()
            .parse()
            .unwrap();

        let neighbours = parts
            .next()
            .unwrap()
            .get("tunnel leads to valves".len()..)
            .unwrap()
            .split(", ")
            .map(|el| el.trim().to_string())
            .collect::<Vec<String>>();

        Valve {
            id,
            flow_rate,
            neighbours,
        }
    }

    fn has_no_rate(&self) -> bool {
        self.flow_rate == 0
    }

    fn pressure_released(&self, minutes: &u32) -> u32 {
        self.flow_rate * minutes
    }
}

struct Path<'a>(Vec<&'a Valve>);

impl<'a> Path<'a> {
    fn pressure_potential(&self, minutes_left: &u32, open_valves: &'a HashSet<&'a String>) -> u32 {
        let destination = self.0.last().unwrap();
        if open_valves.contains(&destination.id) {
            return 0;
        }
        let cost_to_move = self.0.len() as u32;
        let cost_to_open = cost_to_move + 1;
        if cost_to_open >= *minutes_left {
            return 0;
        }

        destination.pressure_released(&(minutes_left - cost_to_open))
    }

    fn destination(&self) -> &'a Valve {
        self.0.last().unwrap()
    }

    fn as_decisions(&self) -> Vec<Decision<'a>> {
        let mut decisions: Vec<Decision<'a>> =
            self.0.iter().map(|v| Decision::MoveTo(&v.id)).collect();
        decisions.push(Decision::Open);

        decisions
    }
}

#[derive(PartialEq, Eq)]
enum Decision<'a> {
    MoveTo(&'a String),
    Open,
    Stay,
}

struct Network {
    starting_valve_id: String,
    valves: HashMap<String, Valve>,
}

impl Network {
    fn from_str(lines: &str) -> Network {
        let mut valves: HashMap<String, Valve> = HashMap::new();
        let mut lines = lines.split("\n");
        let starting_valve: Valve = Valve::from_str(lines.next().unwrap());
        let starting_valve_id = starting_valve.id.clone();
        valves.insert(starting_valve.id.clone(), starting_valve);
        for line in lines {
            let valve = Valve::from_str(line);
            valves.insert(valve.id.clone(), valve);
        }

        Network {
            valves,
            starting_valve_id,
        }
    }

    pub fn release_pressure_path<'n>(&'n self, minutes: &u32) -> Vec<Decision<'n>> {
        let best_network_paths: HashMap<&'n String, Vec<Path<'n>>> = self.best_network_paths();
        let mut decisions: Vec<Vec<Decision<'n>>> = vec![];
        for force_valve in self.valves.keys().filter(|v| v != &&self.starting_valve_id) {
            decisions.push(self.release_pressure_path_by_first_valve(
                force_valve,
                minutes,
                &best_network_paths,
            ));
        }

        let best = decisions
            .iter()
            .enumerate()
            .map(|(i, d)| (i, d, self.pressure_released(d)))
            .max_by(|a, b| a.2.cmp(&b.2))
            .unwrap()
            .0;

        decisions.remove(best)
    }

    fn release_pressure_path_by_first_valve<'n>(
        &'n self,
        first_valve: &'n String,
        minutes: &u32,
        best_network_paths: &HashMap<&'n String, Vec<Path<'n>>>,
    ) -> Vec<Decision<'n>> {
        let mut open_valves: HashSet<&'n String> = self
            .valves
            .values()
            .filter(|v| v.has_no_rate())
            .map(|v| &v.id)
            .collect();

        let mut decisions: Vec<Decision<'n>> = best_network_paths
            .get(&self.starting_valve_id)
            .unwrap()
            .iter()
            .filter(|p| &p.destination().id == first_valve)
            .nth(0)
            .unwrap()
            .as_decisions();
        open_valves.insert(first_valve);
        let mut current_id = first_valve;
        let mut current_minute = decisions.len() as u32;
        while current_minute <= *minutes {
            let (best_path, best_potential) = best_network_paths
                .get(current_id)
                .unwrap()
                .iter()
                .map(|p| {
                    (
                        p,
                        p.pressure_potential(&(minutes - current_minute), &open_valves),
                    )
                })
                .max_by(|a, b| a.1.cmp(&b.1))
                .expect("should have a max value");
            if best_potential == 0 {
                decisions.push(Decision::Stay);
                current_minute += 1;
            } else {
                let mut current_decisions = best_path.as_decisions();
                current_minute += current_decisions.len() as u32;
                decisions.append(&mut current_decisions);
                current_id = &best_path.destination().id;
                open_valves.insert(current_id);
            }
        }

        decisions
    }

    pub fn pressure_released<'n>(&self, decisions: &'n Vec<Decision<'n>>) -> Vec<u32> {
        let mut current_valve_id = &self.starting_valve_id;
        let mut current_pressure: u32 = 0;
        let mut pressures: Vec<u32> = vec![];
        for m in 0..=decisions.len() - 1 {
            pressures.push(current_pressure);
            match decisions.get(m).unwrap() {
                Decision::MoveTo(valve_id) => {
                    current_valve_id = valve_id;
                }
                Decision::Open => {
                    current_pressure += self.valves.get(current_valve_id).unwrap().flow_rate;
                }
                Decision::Stay => {}
            }
        }

        pressures
    }

    fn best_network_paths<'n>(&'n self) -> HashMap<&'n String, Vec<Path<'n>>> {
        let mut network_paths: HashMap<&'n String, Vec<Path<'n>>> = HashMap::new();
        for valve_id in self.valves.keys() {
            network_paths.insert(valve_id, self.best_valve_paths(valve_id));
        }

        network_paths
    }

    fn best_valve_paths<'n>(&'n self, valve_id: &'n String) -> Vec<Path<'n>> {
        let mut unvisited: HashSet<&'n String> = HashSet::new();
        let mut costs: HashMap<&'n String, u16> = HashMap::new();
        let mut paths: HashMap<&'n String, &'n String> = HashMap::new();
        for v in self.valves.keys() {
            costs.insert(v, u16::MAX);
            unvisited.insert(v);
        }
        costs.insert(valve_id, 0);

        while let Some(current_id) = self.find_smallest_unvisited(&costs, &unvisited) {
            unvisited.remove(current_id);
            for neighbour in
                self.find_unvisited_neighbours(self.valves.get(current_id).unwrap(), &unvisited)
            {
                let neighbour_cost = *costs.get(neighbour).unwrap();
                let new_cost = costs.get(current_id).unwrap() + 1;
                if new_cost < neighbour_cost {
                    costs.insert(neighbour, new_cost);
                    paths.insert(neighbour, current_id);
                }
            }
        }

        paths
            .keys()
            .map(|destination| self.collect_path(destination, &paths))
            .collect()
    }

    fn find_smallest_unvisited<'n>(
        &'n self,
        costs: &HashMap<&'n String, u16>,
        unvisited: &HashSet<&'n String>,
    ) -> Option<&'n String> {
        let mut min_cost = &u16::MAX;
        let mut min_valve: Option<&'n String> = None;
        for u in unvisited.iter() {
            let u_cost = costs.get(u).unwrap();
            if u_cost < min_cost {
                min_cost = u_cost;
                min_valve = Some(u.clone());
            }
        }

        min_valve
    }

    fn find_unvisited_neighbours<'n>(
        &'n self,
        valve: &'n Valve,
        unvisited: &HashSet<&'n String>,
    ) -> Vec<&'n String> {
        valve
            .neighbours
            .iter()
            .filter(|v| unvisited.contains(v))
            .collect()
    }

    fn collect_path<'n>(
        &'n self,
        destination: &'n String,
        paths: &HashMap<&'n String, &'n String>,
    ) -> Path<'n> {
        let mut path: Vec<&'n Valve> = vec![self.valves.get(destination).unwrap()];
        while let Some(&prev) = paths.get(&path.last().unwrap().id) {
            path.push(self.valves.get(prev).unwrap());
        }
        path.pop();

        Path(path.into_iter().rev().collect())
    }
}

fn test() {
    println!("[Day 16] Tests");
    let (aa, bb, cc, dd, ee, ff, gg, hh, ii, jj) = (
        "AA".to_string(),
        "BB".to_string(),
        "CC".to_string(),
        "DD".to_string(),
        "EE".to_string(),
        "FF".to_string(),
        "GG".to_string(),
        "HH".to_string(),
        "II".to_string(),
        "JJ".to_string(),
    );

    println!("[Tests] Valve parsing");
    println!(
        "{}",
        Valve::from_str("Valve AA has flow rate=16; tunnels lead to valves DD, II, BB")
            == Valve {
                id: aa.clone(),
                flow_rate: 16,
                neighbours: vec![dd.clone(), ii.clone(), bb.clone()]
            }
    );
    println!(
        "{}",
        // lead to valve (without s)
        Valve::from_str("Valve HH has flow rate=8; tunnels lead to valve AA")
            == Valve {
                id: hh.clone(),
                flow_rate: 8,
                neighbours: vec![aa.clone()]
            }
    );

    println!("[Tests] Network pressure");
    let network = read_file("test.txt");
    let decisions = network.release_pressure_path(&30);
    let expected_decisions = vec![
        Decision::MoveTo(&dd),
        Decision::Open,
        Decision::MoveTo(&cc),
        Decision::MoveTo(&bb),
        Decision::Open,
        Decision::MoveTo(&aa),
        Decision::MoveTo(&ii),
        Decision::MoveTo(&jj),
        Decision::Open,
        Decision::MoveTo(&ii),
        Decision::MoveTo(&aa),
        Decision::MoveTo(&dd),
        Decision::MoveTo(&ee),
        Decision::MoveTo(&ff),
        Decision::MoveTo(&gg),
        Decision::MoveTo(&hh),
        Decision::Open,
        Decision::MoveTo(&gg),
        Decision::MoveTo(&ff),
        Decision::MoveTo(&ee),
        Decision::Open,
        Decision::MoveTo(&dd),
        Decision::MoveTo(&cc),
        Decision::Open,
        Decision::Stay,
        Decision::Stay,
        Decision::Stay,
        Decision::Stay,
        Decision::Stay,
        Decision::Stay,
    ];
    for d in decisions.iter() {
        match d {
            Decision::MoveTo(i) => println!("MoveTo {}", i),
            Decision::Open => println!("Open"),
            Decision::Stay => println!("Stay"),
        }
    }
    println!("{}", decisions == expected_decisions);
    println!(
        "{}",
        network.pressure_released(&decisions).iter().sum::<u32>()
    );
    println!(
        "{}",
        network
            .pressure_released(&expected_decisions)
            .iter()
            .sum::<u32>()
            == 1651
    );

    println!("");
}

fn read_file(path: &str) -> Network {
    Network::from_str(&fs::read_to_string(path).expect("Should be able to load file"))
}

fn solve() {
    println!("[Day 16] Solving");

    let network = read_file("input.txt");
}

fn main() {
    test();

    solve();
}
