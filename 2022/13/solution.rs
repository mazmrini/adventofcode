use std::cmp;
use std::fs;

#[derive(Eq, PartialEq)]
enum Packet {
    Unit(u8),
    Packet(Box<Vec<Packet>>),
}

#[derive(Eq, PartialEq)]
struct PacketPair {
    left: Packet,
    right: Packet,
}

impl Packet {
    fn from_strs(value: &str) -> Vec<Packet> {
        value.split("\n").map(|el| Packet::from_str(el)).collect()
    }

    fn from_str(value: &str) -> Packet {
        if let Ok(v) = value.parse::<u8>() {
            return Packet::Unit(v);
        }
        if value == "[]" {
            return Packet::Packet(Box::new(vec![]));
        }
        let mut packets: Vec<String> = vec![];
        let mut current_str = String::new();
        let mut open: u8 = 0;
        for c in value.get(1..value.len() - 1).unwrap().chars() {
            if open == 0 && c == ',' {
                packets.push(current_str);
                current_str = String::new();
            } else {
                if c == '[' {
                    open += 1;
                } else if c == ']' {
                    open -= 1;
                }
                current_str.push(c);
            }
        }
        packets.push(current_str);

        Packet::Packet(Box::new(
            packets.iter().map(|el| Packet::from_str(el)).collect(),
        ))
    }

    fn to_packet(unit: u8) -> Packet {
        Packet::Packet(Box::new(vec![Packet::Unit(unit)]))
    }

    fn is_in_order_with(&self, next: &Packet) -> Option<bool> {
        match self {
            Packet::Unit(s) => match next {
                Packet::Unit(n) => {
                    if s < n {
                        return Some(true);
                    } else if n < s {
                        return Some(false);
                    }

                    return None;
                }
                Packet::Packet(_n) => return Packet::to_packet(*s).is_in_order_with(next),
            },
            Packet::Packet(s) => match next {
                Packet::Unit(n) => return self.is_in_order_with(&Packet::to_packet(*n)),
                Packet::Packet(n) => {
                    let max_len = cmp::min(s.len(), n.len());
                    for i in 0..max_len {
                        if let Some(b) = s
                            .get(i)
                            .expect("self packet")
                            .is_in_order_with(n.get(i).expect("next packet"))
                        {
                            return Some(b);
                        }
                    }
                    if s.len() > n.len() {
                        return Some(false);
                    } else if n.len() > s.len() {
                        return Some(true);
                    }

                    return None;
                }
            },
        }
    }

    fn cmp(&self, other: &Packet) -> cmp::Ordering {
        let order = self.is_in_order_with(other);
        match order {
            None => cmp::Ordering::Equal,
            Some(b) => {
                if b {
                    return cmp::Ordering::Less;
                }

                cmp::Ordering::Greater
            }
        }
    }
}

impl PacketPair {
    fn from_strs(value: &str) -> Vec<PacketPair> {
        value
            .split("\n\n")
            .map(|el| PacketPair::from_str(el))
            .collect()
    }

    fn from_str(value: &str) -> PacketPair {
        let mut splits = value.split("\n");

        PacketPair {
            left: Packet::from_str(splits.next().unwrap()),
            right: Packet::from_str(splits.next().unwrap()),
        }
    }

    fn is_in_order(&self) -> Option<bool> {
        self.left.is_in_order_with(&self.right)
    }
}

fn right_order_score(pairs: &Vec<PacketPair>) -> u16 {
    pairs
        .iter()
        .enumerate()
        .map(|(i, p)| (i, p.is_in_order()))
        .filter(|(_i, p)| p == &Some(true))
        .map(|(i, _p)| (i + 1) as u16)
        .sum::<u16>()
}

fn distress_signal(packets: &mut Vec<Packet>) -> u16 {
    let first_divider = &Packet::from_str("[[2]]");
    let second_divider = &Packet::from_str("[[6]]");
    packets.sort_by(|a, b| a.cmp(b));

    let mut total: u16 = 1;
    for i in packets
        .iter()
        .enumerate()
        .filter(|(_i, p)| *p == first_divider || *p == second_divider)
        .map(|(i, _p)| (i + 1) as u16)
    {
        total *= i;
    }

    total
}

fn test() {
    println!("[Day 13] Tests");
    let packet_pairs = read_file("test.txt");
    println!("[Tests] Deserialization");
    println!(
        "{}",
        packet_pairs.get(0).unwrap().left
            == Packet::Packet(Box::new(vec![
                Packet::Unit(1),
                Packet::Unit(1),
                Packet::Unit(3),
                Packet::Unit(1),
                Packet::Unit(1),
            ]))
    );
    println!(
        "{}",
        packet_pairs.get(2).unwrap().left == Packet::Packet(Box::new(vec![Packet::Unit(9)]))
    );
    println!(
        "{}",
        packet_pairs.get(2).unwrap().right
            == Packet::Packet(Box::new(vec![Packet::Packet(Box::new(vec![
                Packet::Unit(8),
                Packet::Unit(7),
                Packet::Unit(6),
            ]))]))
    );
    println!(
        "{}",
        packet_pairs.get(6).unwrap().left
            == Packet::Packet(Box::new(vec![Packet::Packet(Box::new(vec![
                Packet::Packet(Box::new(vec![]))
            ]))]))
    );
    println!(
        "{}",
        Packet::from_str("[[8,5],10,[5,8]]")
            == Packet::Packet(Box::new(vec![
                Packet::Packet(Box::new(vec![Packet::Unit(8), Packet::Unit(5)])),
                Packet::Unit(10),
                Packet::Packet(Box::new(vec![Packet::Unit(5), Packet::Unit(8)])),
            ]))
    );

    println!("[Tests] #is_in_order");
    println!(
        "{}",
        packet_pairs.get(0).unwrap().is_in_order() == Some(true)
    );
    println!(
        "{}",
        packet_pairs.get(1).unwrap().is_in_order() == Some(true)
    );
    println!(
        "{}",
        packet_pairs.get(2).unwrap().is_in_order() == Some(false)
    );
    println!(
        "{}",
        packet_pairs.get(3).unwrap().is_in_order() == Some(true)
    );
    println!(
        "{}",
        packet_pairs.get(4).unwrap().is_in_order() == Some(false)
    );
    println!(
        "{}",
        packet_pairs.get(5).unwrap().is_in_order() == Some(true)
    );
    println!(
        "{}",
        packet_pairs.get(6).unwrap().is_in_order() == Some(false)
    );
    println!(
        "{}",
        packet_pairs.get(7).unwrap().is_in_order() == Some(false)
    );

    println!("[Tests] #right_order_score");
    println!("{}", right_order_score(&packet_pairs) == 13);

    println!("[Tests] #distress_signal");
    let mut packets = read_file_part_2("test.txt");
    println!("{}", distress_signal(&mut packets) == 140);

    println!("");
}

fn read_file(path: &str) -> Vec<PacketPair> {
    PacketPair::from_strs(&fs::read_to_string(path).expect("Should be able to load file"))
}

fn read_file_part_2(path: &str) -> Vec<Packet> {
    let mut content = fs::read_to_string(path)
        .expect("Should exist")
        .replace("\n\n", "\n");
    content.push_str("\n[[2]]");
    content.push_str("\n[[6]]");

    Packet::from_strs(&content)
}

fn solve() {
    println!("[Day 13] Solving");

    let pairs = read_file("input.txt");
    println!("Score: {}", right_order_score(&pairs));

    let mut packets = read_file_part_2("input.txt");
    println!("Distress signal: {}", distress_signal(&mut packets));
}

fn main() {
    test();

    solve();
}
