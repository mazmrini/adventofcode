// A,X : rock (1)
// B,Y : paper (2)
// C,Z : scissors (3)

// win = 6, draw = 3
use std::collections::HashMap;
use std::fs;

const ROCK: &str = "R";
const PAPER: &str = "P";
const SCISSORS: &str = "S";
const WIN: &str = "W";
const DRAW: &str = "D";
const LOSS: &str = "L";

fn points_map() -> HashMap<&'static str, i32> {
    HashMap::from([
        (WIN, 6),
        (DRAW, 3),
        (LOSS, 0),
        (ROCK, 1),
        (PAPER, 2),
        (SCISSORS, 3),
    ])
}

fn result_map() -> HashMap<&'static str, &'static str> {
    HashMap::from([
        ("RvS", WIN),
        ("RvR", DRAW),
        ("RvP", LOSS),
        ("PvR", WIN),
        ("PvP", DRAW),
        ("PvS", LOSS),
        ("SvP", WIN),
        ("SvS", DRAW),
        ("SvR", LOSS),
    ])
}

fn input_map() -> HashMap<&'static str, &'static str> {
    HashMap::from([
        ("A", ROCK),
        ("B", PAPER),
        ("C", SCISSORS),
        ("X", ROCK),
        ("Y", PAPER),
        ("Z", SCISSORS),
    ])
}

fn decision_map() -> HashMap<&'static str, HashMap<&'static str, &'static str>> {
    HashMap::from([
        ("A", HashMap::from([("X", "Z"), ("Y", "X"), ("Z", "Y")])),
        ("B", HashMap::from([("X", "X"), ("Y", "Y"), ("Z", "Z")])),
        ("C", HashMap::from([("X", "Y"), ("Y", "Z"), ("Z", "X")])),
    ])
}

struct GameMatch {
    me: String,
    opponent: String,
}

impl GameMatch {
    fn new(me: &str, opponent: &str) -> GameMatch {
        GameMatch {
            me: me.to_string(),
            opponent: opponent.to_string(),
        }
    }
}

struct Game {
    points_map: HashMap<&'static str, i32>,
    input_map: HashMap<&'static str, &'static str>,
    result_map: HashMap<&'static str, &'static str>,
    decision_map: HashMap<&'static str, HashMap<&'static str, &'static str>>,
}

impl Game {
    fn new() -> Game {
        Game {
            points_map: points_map(),
            input_map: input_map(),
            result_map: result_map(),
            decision_map: decision_map(),
        }
    }

    fn play(&self, matches: &Vec<GameMatch>) -> i32 {
        matches
            .iter()
            .map(|m| self.compute_points(&*m.me, &*m.opponent))
            .sum::<i32>()
    }

    fn decide(&self, matches: &Vec<GameMatch>) -> i32 {
        matches
            .iter()
            .map(|m| {
                let me = self
                    .decision_map
                    .get(&*m.opponent)
                    .expect("op not found")
                    .get(&*m.me)
                    .expect("me not found");

                self.compute_points(me, &*m.opponent)
            })
            .sum::<i32>()
    }

    fn compute_points(&self, me: &str, opponent: &str) -> i32 {
        let me = self.input_map.get(me).expect("me not found");
        let op = self.input_map.get(opponent).expect("opponent not found");

        let result_key = format!("{}v{}", me, op);
        let game_result = self.result_map.get(&*result_key).expect("result not found");

        let me_points = self.points_map.get(me).expect("unable to find me play");
        let game_points = self
            .points_map
            .get(game_result)
            .expect("unable to find game result");

        me_points + game_points
    }
}

fn test() {
    let me_rock = "X";
    let me_paper = "Y";
    let me_scissors = "Z";
    let away_rock = "A";
    let away_paper = "B";
    let away_scissors = "C";

    let game = Game::new();
    // ROCK
    println!(
        "{}",
        game.play(&vec![GameMatch::new(me_rock, away_rock)]) == 1 + 3
    );
    println!(
        "{}",
        game.play(&vec![GameMatch::new(me_rock, away_scissors)]) == 1 + 6
    );
    println!(
        "{}",
        game.play(&vec![GameMatch::new(me_paper, away_paper)]) == 2 + 3
    );
    println!(
        "{}",
        game.play(&vec![GameMatch::new(me_scissors, away_rock)]) == 3 + 0
    );
    println!(
        "{}",
        game.play(&vec![
            GameMatch::new(me_rock, away_rock),
            GameMatch::new(me_paper, away_paper),
            GameMatch::new(me_scissors, away_scissors),
        ]) == (1 + 3) + (2 + 3) + (3 + 3)
    );
    // DECISIONS
    let me_loss = me_rock;
    let me_draw = me_paper;
    let me_win = me_scissors;
    println!(
        "{}",
        game.decide(&vec![GameMatch::new(me_loss, away_rock)]) == 0 + 3
    );
    println!(
        "{}",
        game.decide(&vec![GameMatch::new(me_draw, away_scissors)]) == 3 + 3
    );
    println!(
        "{}",
        game.decide(&vec![GameMatch::new(me_win, away_rock)]) == 6 + 2
    );
    println!(
        "{}",
        game.decide(&vec![GameMatch::new(me_win, away_scissors)]) == 6 + 1
    );
    println!(
        "{}",
        game.decide(&vec![
            GameMatch::new(me_win, away_rock),
            GameMatch::new(me_draw, away_paper),
            GameMatch::new(me_loss, away_scissors),
        ]) == (6 + 2) + (3 + 2) + (0 + 2)
    );

    println!("");
}

fn read_file(path: &str) -> Vec<GameMatch> {
    fs::read_to_string(path)
        .expect("Should've been able to read file")
        .split("\n")
        .map(|el| {
            let splits = el.split(" ").collect::<Vec<&str>>();
            let opponent = splits.get(0).unwrap();
            let me = splits.get(1).unwrap();
            GameMatch::new(me, opponent)
        })
        .collect::<Vec<GameMatch>>()
}

fn solve() {
    let matches = read_file("input.txt");
    let game = Game::new();

    println!("Play: {}", game.play(&matches));
    println!("Decide: {}", game.decide(&matches));
}

fn main() {
    test();

    solve();
}
