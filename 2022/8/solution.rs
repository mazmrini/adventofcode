use std::collections::HashSet;
use std::convert::TryInto;
use std::fs;

struct Forest {
    nb_lines: usize,
    nb_cols: usize,
    trees: Vec<u8>,
}

impl Forest {
    fn from_str(value: String) -> Forest {
        let mut nb_lines: usize = 1;
        let mut nb_cols: usize = 0;
        let mut trees: Vec<u8> = vec![];
        for c in value.chars() {
            if c == '\n' {
                nb_cols = 0;
                nb_lines += 1;
            } else {
                nb_cols += 1;
                trees.push(String::from(c).parse::<u8>().expect("should be a number"));
            }
        }

        Forest {
            nb_lines,
            nb_cols,
            trees,
        }
    }

    fn count_visible(&self) -> usize {
        let nb_outer_visible: usize = (2 * (self.nb_cols + self.nb_lines - 2))
            .try_into()
            .expect("should be OK");

        let mut visible: HashSet<usize> = HashSet::new();
        for i in 1..self.nb_lines - 1 {
            let mut l_max: u8 = *self.trees.get(self.to_idx(i, 0)).expect("l_max to exist");
            let mut r_max: u8 = *self
                .trees
                .get(self.to_idx(i, self.nb_cols - 1))
                .expect("r_max to exist");
            for j in 1..self.nb_cols - 1 {
                let l_idx = self.to_idx(i, j);
                let l_value = *self.trees.get(l_idx).unwrap();
                let r_idx = self.to_idx(i, self.nb_cols - 1 - j);
                let r_value = *self.trees.get(r_idx).unwrap();
                if l_value > l_max {
                    visible.insert(l_idx);
                    l_max = l_value;
                }
                if r_value > r_max {
                    visible.insert(r_idx);
                    r_max = r_value;
                }
            }
        }
        for j in 1..self.nb_cols - 1 {
            let mut t_max: u8 = *self.trees.get(self.to_idx(0, j)).expect("t_max to exist");
            let mut b_max: u8 = *self
                .trees
                .get(self.to_idx(self.nb_lines - 1, j))
                .expect("b_max to exist");
            for i in 1..self.nb_lines - 1 {
                let t_idx = self.to_idx(i, j);
                let t_value = *self.trees.get(t_idx).unwrap();
                let b_idx = self.to_idx(self.nb_lines - 1 - i, j);
                let b_value = *self.trees.get(b_idx).unwrap();
                if t_value > t_max {
                    visible.insert(t_idx);
                    t_max = t_value;
                }
                if b_value > b_max {
                    visible.insert(b_idx);
                    b_max = b_value;
                }
            }
        }

        nb_outer_visible + visible.len()
    }

    fn scenic_scores(&self) -> Vec<u32> {
        self.granular_scenic_scores()
            .into_iter()
            .map(|(t, r, b, l)| t * r * b * l)
            .collect()
    }

    fn granular_scenic_scores(&self) -> Vec<(u32, u32, u32, u32)> {
        self.trees
            .iter()
            .enumerate()
            .map(|(i, height)| self.scenic_score_of(i, height))
            .collect()
    }

    // top right bot left
    fn scenic_score_of(&self, trees_idx: usize, height: &u8) -> (u32, u32, u32, u32) {
        let (idx_i, idx_j) = self.to_i_j(trees_idx);
        let mut left_score = 0;
        let mut right_score = 0;
        let mut top_score = 0;
        let mut bottom_score = 0;
        for left in (0..idx_j).rev() {
            left_score += 1;
            if self.get_tree(idx_i, left).unwrap() >= height {
                break;
            }
        }
        for right in idx_j + 1..self.nb_cols {
            right_score += 1;
            if self.get_tree(idx_i, right).unwrap() >= height {
                break;
            }
        }
        for top in (0..idx_i).rev() {
            top_score += 1;
            if self.get_tree(top, idx_j).unwrap() >= height {
                break;
            }
        }
        for bot in idx_i + 1..self.nb_lines {
            bottom_score += 1;
            if self.get_tree(bot, idx_j).unwrap() >= height {
                break;
            }
        }

        (top_score, right_score, bottom_score, left_score)
    }

    fn to_i_j(&self, idx: usize) -> (usize, usize) {
        let i = idx / self.nb_cols;
        (i, idx - self.nb_cols * i)
    }

    fn to_idx(&self, i: usize, j: usize) -> usize {
        i * self.nb_cols + j % self.nb_cols
    }

    fn get_tree(&self, i: usize, j: usize) -> Option<&u8> {
        self.trees.get(self.to_idx(i, j))
    }
}

fn test() {
    println!("[Day 8] Tests");
    let forest = Forest::from_str("30373\n25512\n65332\n33549\n35390".to_string());
    println!("{}", forest.count_visible() == 21);
    println!("{}", forest.scenic_scores().iter().max() == Some(&8));

    let forest = Forest::from_str("38373\n25416\n65332\n93549\n35320".to_string());
    println!("{}", forest.count_visible() == 22);

    println!("")
}

fn read_file(path: &str) -> Forest {
    Forest::from_str(fs::read_to_string(path).expect("Should be able to load file"))
}

fn solve() {
    println!("[Day 8] Solving");
    let forest = read_file("input.txt");

    println!("Visible trees: {}", forest.count_visible());
    println!(
        "Best scenic score: {}",
        forest.scenic_scores().iter().max().unwrap()
    );
}

fn main() {
    test();

    solve();
}
