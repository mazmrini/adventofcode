use std::fs;

struct Range {
    left: i16,
    right: i16,
}

impl Range {
    fn new(left: i16, right: i16) -> Range {
        Range { left, right }
    }

    fn from_str(value: &str) -> Range {
        let mut split = value.split("-");
        let (left, right) = (split.next().unwrap(), split.next().unwrap());

        Range {
            left: left.parse::<i16>().expect("should parse"),
            right: right.parse::<i16>().expect("should parse"),
        }
    }

    fn can_hold(&self, other: &Range) -> bool {
        self.left <= other.left && self.right >= other.right
    }

    fn contains_any(&self, other: &Range) -> bool {
        !(self.left > other.right || self.right < other.left)
    }
}

struct Group {
    left: Range,
    right: Range,
}

impl Group {
    fn from_str(value: &str) -> Group {
        let mut split = value.split(",");
        let (left, right) = (split.next().unwrap(), split.next().unwrap());

        Group {
            left: Range::from_str(left),
            right: Range::from_str(right),
        }
    }

    fn has_fully_duped_work(&self) -> bool {
        return self.left.can_hold(&self.right) || self.right.can_hold(&self.left);
    }

    fn has_duped_work(&self) -> bool {
        return self.left.contains_any(&self.right) || self.right.contains_any(&self.left);
    }
}

fn test() {
    let range = Range::new(10, 30);
    println!("{}", range.can_hold(&Range::new(10, 30)) == true);
    println!("{}", range.can_hold(&Range::new(10, 20)) == true);
    println!("{}", range.can_hold(&Range::new(15, 20)) == true);
    println!("{}", range.can_hold(&Range::new(15, 30)) == true);

    println!("{}", range.can_hold(&Range::new(5, 9)) == false);
    println!("{}", range.can_hold(&Range::new(5, 10)) == false);
    println!("{}", range.can_hold(&Range::new(5, 15)) == false);
    println!("{}", range.can_hold(&Range::new(5, 30)) == false);
    println!("{}", range.can_hold(&Range::new(5, 35)) == false);
    println!("{}", range.can_hold(&Range::new(10, 35)) == false);
    println!("{}", range.can_hold(&Range::new(15, 35)) == false);
    println!("{}", range.can_hold(&Range::new(30, 35)) == false);
    println!("{}", range.can_hold(&Range::new(31, 35)) == false);

    println!("{}", range.contains_any(&Range::new(5, 10)) == true);
    println!("{}", range.contains_any(&Range::new(5, 15)) == true);
    println!("{}", range.contains_any(&Range::new(10, 30)) == true);
    println!("{}", range.contains_any(&Range::new(10, 20)) == true);
    println!("{}", range.contains_any(&Range::new(15, 20)) == true);
    println!("{}", range.contains_any(&Range::new(15, 30)) == true);
    println!("{}", range.contains_any(&Range::new(15, 35)) == true);
    println!("{}", range.contains_any(&Range::new(30, 35)) == true);

    println!("{}", range.contains_any(&Range::new(5, 9)) == false);
    println!("{}", range.contains_any(&Range::new(31, 35)) == false);

    println!("");
}

fn count_fully_overlapping(data: &Vec<Group>) -> usize {
    data.iter().filter(|g| g.has_fully_duped_work()).count()
}

fn count_overlapping(data: &Vec<Group>) -> usize {
    data.iter().filter(|g| g.has_duped_work()).count()
}

fn read_file(path: &str) -> Vec<Group> {
    fs::read_to_string(path)
        .expect("Should be able to load file")
        .split("\n")
        .map(|el| Group::from_str(el))
        .collect()
}

fn solve() {
    let data = read_file("input.txt");

    println!("Count fully: {}", count_fully_overlapping(&data));
    println!("Count some:  {}", count_overlapping(&data));
}

fn main() {
    test();

    solve();
}
