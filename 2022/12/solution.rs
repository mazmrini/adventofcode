use std::collections::HashMap;
use std::collections::HashSet;
use std::fs;

fn char_height_map() -> HashMap<char, i16> {
    let mut map = HashMap::from([('S', 0), ('E', 25)]);
    for (i, c) in "abcdefghijklmnopqrstuvwxyz".chars().enumerate() {
        map.insert(c, i as i16);
    }

    map
}

fn inv_char_height_map() -> HashMap<char, i16> {
    let mut map: HashMap<char, i16> = HashMap::new();
    for (c, height) in &char_height_map() {
        map.insert(*c, 25 - height);
    }

    map
}

struct InterestPoints {
    source: (i16, i16),
    destination: (i16, i16),
}

struct HeightMap {
    points: HashMap<(i16, i16), i16>,
}

impl HeightMap {
    fn from_str(value: &str, height_map: &HashMap<char, i16>) -> (HeightMap, InterestPoints) {
        let mut source: (i16, i16) = (0, 0);
        let mut destination: (i16, i16) = (0, 0);
        let mut points: HashMap<(i16, i16), i16> = HashMap::new();
        for (j, line) in value.split("\n").enumerate() {
            for (i, c) in line.chars().enumerate() {
                let p = (i as i16, j as i16);
                points.insert(p.clone(), *height_map.get(&c).unwrap());
                if c == 'S' {
                    source = p.clone();
                } else if c == 'E' {
                    destination = p.clone();
                }
            }
        }

        (
            HeightMap { points },
            InterestPoints {
                source,
                destination,
            },
        )
    }

    fn shortest_path(
        &self,
        source: &(i16, i16),
        destination: &(i16, i16),
    ) -> Option<Vec<(i16, i16)>> {
        let (paths, _costs) = self.dijkstra_paths_costs(source);

        self.find_path(&paths, source, destination)
    }

    fn shortest_path_to_height(
        &self,
        source: &(i16, i16),
        height: &i16,
    ) -> Option<Vec<(i16, i16)>> {
        let (paths, costs) = self.dijkstra_paths_costs(source);
        let correct_height_points: Vec<(&(i16, i16), &u32)> = costs
            .iter()
            .filter(|(point, _cost)| self.points.get(point).unwrap() == height)
            .collect();
        if correct_height_points.is_empty() {
            return None;
        }

        let destination: &(i16, i16) = &correct_height_points
            .iter()
            .min_by(|(_a, a_cost), (_b, b_cost)| a_cost.cmp(b_cost))
            .unwrap()
            .0;

        self.find_path(&paths, source, destination)
    }

    fn dijkstra_paths_costs(
        &self,
        source: &(i16, i16),
    ) -> (HashMap<(i16, i16), (i16, i16)>, HashMap<(i16, i16), u32>) {
        let mut unvisited: HashSet<(i16, i16)> = HashSet::new();
        let mut path: HashMap<(i16, i16), (i16, i16)> = HashMap::new();
        let mut cost: HashMap<(i16, i16), u32> = HashMap::new();
        for (p, _) in &self.points {
            cost.insert(*p, u32::MAX - 1); // hack to not overflow with + 1
            unvisited.insert(*p);
        }
        cost.insert(*source, 0);
        while !unvisited.is_empty() {
            let current = self.smallest_unvisited(&cost, &unvisited);
            let current_height = self.points.get(&current).unwrap();
            let adjacents = self.find_unvisited_adjacents(&current, current_height + 1, &unvisited);
            for adj in adjacents.iter() {
                let adj_cost = cost.get(adj).expect("adj should have cost");
                let new_cost = cost.get(&current).expect("current should have cost") + 1;
                if new_cost < *adj_cost {
                    cost.insert(*adj, new_cost);
                    path.insert(*adj, current);
                }
            }
            unvisited.remove(&current);
        }

        (path, cost)
    }

    fn smallest_unvisited(
        &self,
        cost: &HashMap<(i16, i16), u32>,
        unvisited: &HashSet<(i16, i16)>,
    ) -> (i16, i16) {
        let mut min_cost: &u32 = &u32::MAX;
        let mut min_unvisited: &(i16, i16) = &(0, 0);
        for u in unvisited.iter() {
            let u_cost = cost.get(u).unwrap();
            if u_cost < min_cost {
                min_cost = u_cost;
                min_unvisited = u;
            }
        }

        return *min_unvisited;
    }

    fn find_unvisited_adjacents(
        &self,
        of: &(i16, i16),
        max_height: i16,
        unvisited: &HashSet<(i16, i16)>,
    ) -> Vec<(i16, i16)> {
        vec![
            (of.0 - 1, of.1),
            (of.0 + 1, of.1),
            (of.0, of.1 - 1),
            (of.0, of.1 + 1),
        ]
        .into_iter()
        .filter(|p| self.points.contains_key(p))
        .filter(|p| unvisited.contains(p))
        .filter(|p| self.points.get(p).expect("adj to have height") <= &max_height)
        .collect()
    }

    fn find_path(
        &self,
        path: &HashMap<(i16, i16), (i16, i16)>,
        source: &(i16, i16),
        destination: &(i16, i16),
    ) -> Option<Vec<(i16, i16)>> {
        let mut result: Vec<(i16, i16)> = vec![*destination];
        loop {
            let prev = path.get(result.last().unwrap());
            if prev.is_none() {
                return None;
            }
            if prev.unwrap() == source {
                return Some(result.into_iter().rev().collect());
            } else {
                result.push(*prev.unwrap());
            }
        }
    }
}

fn test() {
    println!("[Day 12] Tests");
    let (height_map, interest_points) = HeightMap::from_str(
        "Sabqponm\nabcryxxl\naccszExk\nacctuvwj\nabdefghi",
        &char_height_map(),
    );

    let shortest_path =
        height_map.shortest_path(&interest_points.source, &interest_points.destination);
    println!("{}", shortest_path.unwrap().len() == 31);

    let (height_map, interest_points) = HeightMap::from_str(
        "Sabqponm\nabcryxxl\naccszExk\nacctuvwj\nabdefghi",
        &inv_char_height_map(),
    );
    let shortest_path = height_map.shortest_path_to_height(&interest_points.destination, &25);
    println!("{}", shortest_path.unwrap().len() == 29);
    println!("");
}

fn read_file(path: &str, height_map: &HashMap<char, i16>) -> (HeightMap, InterestPoints) {
    HeightMap::from_str(
        &fs::read_to_string(path).expect("Should be able to load file"),
        height_map,
    )
}

fn solve() {
    println!("[Day 12] Solving");

    let (height_map, interest_points) = read_file("input.txt", &char_height_map());
    let shortest_path =
        height_map.shortest_path(&interest_points.source, &interest_points.destination);
    println!("Shortest path length: {}", shortest_path.unwrap().len());

    let (height_map, interest_points) = read_file("input.txt", &inv_char_height_map());
    let shortest_path = height_map.shortest_path_to_height(&interest_points.destination, &25);
    println!("Shortest path to a: {}", shortest_path.unwrap().len());
}

fn main() {
    test();

    solve();
}
