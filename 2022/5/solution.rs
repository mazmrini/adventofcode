use std::collections::VecDeque;
use std::fs;

#[derive(PartialEq, Eq)]
struct Instruction {
    mov: usize,
    from: usize,
    to: usize,
}

impl Instruction {
    // move xx from ii to jj
    // 0    1  2    3  4  5
    fn from_str(value: &str) -> Instruction {
        let parts = value.split(' ').collect::<Vec<&str>>();
        Instruction {
            mov: parts.get(1).expect("mov part").parse::<usize>().unwrap(),
            from: parts.get(3).expect("from part").parse::<usize>().unwrap() - 1usize,
            to: parts.get(5).expect("to part").parse::<usize>().unwrap() - 1usize,
        }
    }
}

#[derive(PartialEq, Eq)]
struct Crane {
    contents: Vec<VecDeque<char>>,
}

impl Crane {
    //  [D]
    //   1
    fn from_str(values: &Vec<&str>) -> Crane {
        let mut contents = values
            .last()
            .expect("should contain a number line")
            .split("   ")
            .map(|_i| VecDeque::new())
            .collect::<Vec<VecDeque<char>>>();

        for i in 0..contents.len() {
            let char_index = 1 + 3 * i + i;
            for j in 0..values.len() - 1 {
                if let Some(v) = values[j].chars().nth(char_index) {
                    if v != ' ' {
                        contents[i].push_front(v)
                    }
                }
            }
        }

        return Crane { contents: contents };
    }

    fn execute_9000(&mut self, instructions: &Vec<Instruction>) {
        for instruction in instructions {
            for _i in 0..instruction.mov {
                let transfer = self
                    .contents
                    .get_mut(instruction.from)
                    .expect("from instruction should be valid")
                    .pop_back()
                    .expect("mov instruction should be valid");

                self.contents
                    .get_mut(instruction.to)
                    .expect("from instruction should be valid")
                    .push_back(transfer);
            }
        }
    }

    fn execute_9001(&mut self, instructions: &Vec<Instruction>) {
        for instruction in instructions {
            let from_length = self
                .contents
                .get(instruction.from)
                .expect("from instruction should be valid")
                .len();
            let mut mov = self
                .contents
                .get_mut(instruction.from)
                .expect("from instruction should be valid")
                .split_off(from_length - instruction.mov);

            self.contents
                .get_mut(instruction.to)
                .expect("from instruction should be valid")
                .append(&mut mov);
        }
    }

    fn top_of_each(&self) -> String {
        let mut s = String::new();
        for c in &self.contents {
            s.push(*c.back().expect("should have one on top"));
        }

        return s;
    }
}

fn test() {
    println!(
        "{}",
        Instruction::from_str("move 1 from 2 to 1")
            == Instruction {
                mov: 1,
                from: 1,
                to: 0
            }
    );
    println!(
        "{}",
        Instruction::from_str("move 15 from 21 to 119")
            == Instruction {
                mov: 15,
                from: 20,
                to: 118
            }
    );
    println!(
        "{}",
        Crane::from_str(&vec!["[D]\n", " 1  "])
            == Crane {
                contents: vec![VecDeque::from(['D'])]
            }
    );

    let instructions = vec![
        Instruction::from_str("move 1 from 2 to 1"),
        Instruction::from_str("move 3 from 1 to 3"),
        Instruction::from_str("move 2 from 2 to 1"),
        Instruction::from_str("move 1 from 1 to 2"),
    ];
    let mut test_crane = Crane::from_str(&vec![
        "    [D]    \n",
        "[N] [C]    \n",
        "[Z] [M] [P]\n",
        " 1   2   3  ",
    ]);
    println!(
        "{}",
        test_crane
            == Crane {
                contents: vec![
                    VecDeque::from(['Z', 'N']),
                    VecDeque::from(['M', 'C', 'D']),
                    VecDeque::from(['P']),
                ]
            }
    );
    println!("{}", test_crane.top_of_each() == "NDP");
    test_crane.execute_9000(&instructions);
    println!("{}", test_crane.top_of_each() == "CMZ");

    let mut test_crane = Crane::from_str(&vec![
        "    [D]    \n",
        "[N] [C]    \n",
        "[Z] [M] [P]\n",
        " 1   2   3  ",
    ]);
    println!("{}", test_crane.top_of_each() == "NDP");
    test_crane.execute_9001(&instructions);
    println!("{}", test_crane.top_of_each() == "MCD");

    println!("");
}

fn read_file(path: &str) -> (Crane, Vec<Instruction>) {
    let data = fs::read_to_string(path).expect("Should be able to load file");
    let mut splits = data.split("\n\n");

    let crane = Crane::from_str(
        &splits
            .next()
            .expect("should have crane")
            .split("\n")
            .collect::<Vec<&str>>(),
    );
    let instructions = splits
        .next()
        .expect("should have instructions")
        .split("\n")
        .map(|line| Instruction::from_str(line))
        .collect::<Vec<Instruction>>();

    (crane, instructions)
}

fn solve_9000() -> String {
    let (mut crane, instructions) = read_file("input.txt");
    crane.execute_9000(&instructions);
    crane.top_of_each()
}

fn solve_9001() -> String {
    let (mut crane, instructions) = read_file("input.txt");
    crane.execute_9001(&instructions);
    crane.top_of_each()
}

fn solve() {
    println!("Top 9000: {}", solve_9000());
    println!("Top 9001: {}", solve_9001());
}

fn main() {
    test();

    solve();
}
