use std::fs;

fn find_max_elf_cals(elves: &Vec<Vec<i32>>) -> i32 {
    let maximum = elves.iter().map(|el| el.iter().sum::<i32>()).max();

    match maximum {
        None => 0,
        Some(maximum) => maximum,
    }
}

fn find_top_three(elves: &Vec<Vec<i32>>) -> i32 {
    if elves.len() < 3 {
        return 0;
    }
    let mut sorted = elves
        .iter()
        .map(|el| el.iter().sum::<i32>())
        .collect::<Vec<i32>>();
    sorted.sort_by(|a, b| b.cmp(a));

    sorted.iter().take(3).sum()
}

fn test() {
    println!("### TESTS ###");
    let data = vec![
        vec![1],       // 1
        vec![1, 2, 3], // 6
        vec![3, 1, 6], // 10
        vec![4, 7, 3], // 14
        vec![10, 1],   // 11
    ];
    println!("{}", find_max_elf_cals(&data) == 14);
    println!("{}", find_top_three(&data) == 35);

    let data = vec![vec![100], vec![10, 20, 30], vec![1, 2, 3]];
    println!("{}", find_max_elf_cals(&data) == 100);
    println!("{}", find_top_three(&data) == 166);

    let data = vec![];
    println!("{}", find_max_elf_cals(&data) == 0);
    println!("{}", find_top_three(&data) == 0);
    println!("### END TESTS ###\n");
}

fn read_file(path: &str) -> Vec<Vec<i32>> {
    fs::read_to_string(path)
        .expect("Should've been able to read file")
        .split("\n\n")
        .map(|el| {
            el.split("\n")
                .map(|i| i.parse::<i32>().unwrap())
                .collect::<Vec<i32>>()
        })
        .collect::<Vec<Vec<i32>>>()
}

fn solve() {
    let elves = read_file("input.txt");

    println!("Max is:   {}", find_max_elf_cals(&elves));
    println!("Top 3 is: {}", find_top_three(&elves));
}

fn main() {
    test();

    solve();
}
