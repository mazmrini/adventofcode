use std::fs;

#[derive(Clone)]
enum Op {
    Square,
    Times(u64),
    Add(u64),
}

#[derive(Clone)]
struct Monkey {
    items: Vec<u64>,
    op: Op,
    divisible_by: u64,
    true_throws_to: usize,
    false_throws_to: usize,
}

impl Monkey {
    fn from_str(value: &str) -> Monkey {
        let mut lines = value.split("\n");
        lines.next().expect("Monkey number");

        let items: Vec<u64> = lines
            .next()
            .expect("starting_items")
            .get("  Starting items: ".len()..)
            .expect("starting items")
            .split(", ")
            .map(|el| el.parse::<u64>().expect("starting items value"))
            .collect();
        let mut op = Op::Square;
        let op_str = lines
            .next()
            .expect("operation")
            .get("  Operation: new = old ".len()..)
            .expect("operation");
        if !op_str.ends_with("* old") {
            let mut ops = op_str.split(" ");
            let symbol = ops.next().expect("operation symbol");
            let value: u64 = ops.next().expect("operand").parse().expect("operand value");
            if symbol == "*" {
                op = Op::Times(value);
            } else {
                op = Op::Add(value);
            }
        }

        let divisible_by: u64 = lines
            .next()
            .expect("Test")
            .get("  Test: divisible by ".len()..)
            .expect("divisible by value")
            .parse()
            .expect("divisible by");
        let true_throws_to: usize = lines
            .next()
            .expect("If true")
            .get("    If true: throw to monkey ".len()..)
            .expect("if true value")
            .parse()
            .expect("true monkey");
        let false_throws_to: usize = lines
            .next()
            .expect("If true")
            .get("    If false: throw to monkey ".len()..)
            .expect("if false value")
            .parse()
            .expect("false monkey");

        Monkey {
            items,
            op,
            divisible_by,
            true_throws_to,
            false_throws_to,
        }
    }

    fn nb_items(&self) -> u64 {
        self.items.len() as u64
    }

    fn inspect(&mut self, reduce_op: &dyn Fn(u64) -> u64) -> Vec<(usize, u64)> {
        let results: Vec<(usize, u64)> = self
            .items
            .iter()
            .map(|item| {
                let mut worry = match self.op {
                    Op::Square => item * item,
                    Op::Add(i) => item + i,
                    Op::Times(i) => item * i,
                };

                worry = reduce_op(worry);
                if worry % self.divisible_by == 0 {
                    return (self.true_throws_to, worry);
                }

                (self.false_throws_to, worry)
            })
            .collect();
        self.items.clear();

        results
    }
}

struct Horde {
    monkeys: Vec<Monkey>,
}

impl Horde {
    fn from_str(value: String) -> Horde {
        Horde {
            monkeys: value.split("\n\n").map(|m| Monkey::from_str(m)).collect(),
        }
    }

    fn congruence_value(&self) -> u64 {
        let mut total: u64 = 1;
        for m in self.monkeys.iter() {
            total *= m.divisible_by;
        }

        total
    }

    fn play(&mut self, rounds: u64, reduce_op: &dyn Fn(u64) -> u64) -> HordeHistory {
        let mut end_of_round: Vec<Vec<Monkey>> = vec![self.monkeys.clone()];
        let mut pre_inspect: Vec<Vec<Monkey>> = vec![];
        for _i in 0..rounds + 1 {
            let mut current_pre_inspect: Vec<Monkey> = vec![];
            for i in 0..self.monkeys.len() {
                current_pre_inspect.push(self.monkeys.get(i).unwrap().clone());
                for (to_monkey, value) in self.monkeys.get_mut(i).unwrap().inspect(reduce_op) {
                    self.monkeys.get_mut(to_monkey).unwrap().items.push(value);
                }
            }
            pre_inspect.push(current_pre_inspect);
            end_of_round.push(self.monkeys.clone());
        }

        HordeHistory::new(end_of_round, pre_inspect)
    }
}

struct HordeHistory {
    end_of_round: Vec<Vec<Monkey>>,
    pre_inspect: Vec<Vec<Monkey>>,
}

impl HordeHistory {
    fn new(end_of_round: Vec<Vec<Monkey>>, pre_inspect: Vec<Vec<Monkey>>) -> HordeHistory {
        HordeHistory {
            end_of_round,
            pre_inspect,
        }
    }

    fn get(&self, i: usize, monkey: usize) -> Option<&Monkey> {
        let horde = self.end_of_round.get(i);
        if horde.is_none() {
            return None;
        }

        horde.unwrap().get(monkey)
    }

    fn stats_by_monkey(&self) -> Vec<u64> {
        let mut stats: Vec<u64> = vec![];

        let nb_rounds = self.pre_inspect.len();
        let nb_monkeys = self.pre_inspect.get(0).unwrap().len();
        for monkey_idx in 0..nb_monkeys {
            stats.push(
                self.pre_inspect
                    .get(..nb_rounds - 1)
                    .unwrap()
                    .iter()
                    .map(|el| el.get(monkey_idx).unwrap().nb_items())
                    .sum(),
            )
        }

        stats
    }

    fn monkey_business_level(&self) -> u64 {
        let mut stats = self.stats_by_monkey();
        stats.sort();
        let stats = stats.into_iter().rev().collect::<Vec<u64>>();

        stats.first().unwrap() * stats.get(1).unwrap()
    }
}

fn part_1_reduce(worry: u64) -> u64 {
    (worry as f64 / 3.0).floor() as u64
}

fn part_2_reduce(horde: &Horde) -> impl Fn(u64) -> u64 {
    let congruence = horde.congruence_value();
    return move |worry: u64| worry % congruence;
}

fn test() {
    let mut horde = read_file("test.txt");
    println!("[Day 11] Tests");

    let history = horde.play(20, &part_1_reduce);
    println!("[Tests] History");
    println!("{}", history.get(0, 0).unwrap().items == vec![79, 98]);
    println!("{}", history.get(0, 2).unwrap().items == vec![79, 60, 97]);
    println!(
        "{}",
        history.get(1, 0).unwrap().items == vec![20, 23, 27, 26]
    );
    println!("{}", history.get(1, 2).unwrap().items == vec![]);
    println!(
        "{}",
        history.get(2, 0).unwrap().items == vec![695, 10, 71, 135, 350]
    );
    println!("{}", history.get(2, 2).unwrap().items == vec![]);
    println!(
        "{}",
        history.get(5, 0).unwrap().items == vec![15, 17, 16, 88, 1037]
    );
    println!(
        "{}",
        history.get(5, 1).unwrap().items == vec![20, 110, 205, 524, 72]
    );
    println!(
        "{}",
        history.get(15, 0).unwrap().items == vec![83, 44, 8, 184, 9, 20, 26, 102]
    );
    println!("{}", history.get(15, 1).unwrap().items == vec![110, 36]);
    println!("{}", history.get(15, 2).unwrap().items == vec![]);
    println!("{}", history.get(15, 3).unwrap().items == vec![]);
    println!(
        "{}",
        history.get(20, 0).unwrap().items == vec![10, 12, 14, 26, 34]
    );
    println!(
        "{}",
        history.get(20, 1).unwrap().items == vec![245, 93, 53, 199, 115]
    );
    println!("{}", history.get(20, 2).unwrap().items == vec![]);
    println!("{}", history.get(20, 3).unwrap().items == vec![]);

    println!("[Tests] History methods");
    println!("{}", history.stats_by_monkey() == [101, 95, 7, 105]);
    println!("{}", history.monkey_business_level() == 10605);

    let mut horde = read_file("test.txt");
    let reduce_fn = part_2_reduce(&horde);
    let history = horde.play(10000, &reduce_fn);
    println!(
        "{}",
        history.stats_by_monkey() == [52166, 47830, 1938, 52013]
    );
    println!("{}", history.monkey_business_level() == 2713310158);

    println!("");
}

fn read_file(path: &str) -> Horde {
    Horde::from_str(fs::read_to_string(path).expect("Should be able to load file"))
}

fn solve() {
    println!("[Day 11] Solving");

    let mut horde = read_file("input.txt");
    let history = horde.play(20, &part_1_reduce);
    println!("Monkey business level: {}", history.monkey_business_level());

    let mut horde = read_file("input.txt");
    let history = horde.play(10000, &part_2_reduce(&horde));
    println!("Monkey business level: {}", history.monkey_business_level());
}

fn main() {
    test();

    solve();
}
