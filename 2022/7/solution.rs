use std::collections::HashMap;
use std::fs;

struct Directory {
    parent: Option<usize>,
    files: HashMap<String, u32>,
    dirs: HashMap<String, usize>,
}

struct FileSystem {
    total_space: u32,
    directories: Vec<Directory>,
}

impl FileSystem {
    fn from_str(value: String, total_space: u32) -> FileSystem {
        let mut lines = value.split("\n");
        lines.next().expect("should contain a root dir");

        let mut fs_dirs: Vec<Directory> = vec![Directory::empty(None)];
        let mut current_idx: usize = 0;
        while let Some(line) = lines.next() {
            if line.starts_with("$ ls") {
                continue;
            } else if line.starts_with("dir ") {
                let (_cmd, dir_name) = line.split_at("dir ".len());
                let fs_idx = fs_dirs.len();
                fs_dirs.push(Directory::empty(Some(current_idx)));
                fs_dirs
                    .get_mut(current_idx)
                    .expect("should exist in fs")
                    .dirs
                    .insert(dir_name.to_string(), fs_idx);
            } else if line.starts_with("$ cd ..") {
                current_idx = fs_dirs
                    .get(current_idx)
                    .expect("should exist in fs")
                    .parent
                    .expect("should have a parent");
            } else if line.starts_with("$ cd ") {
                let (_cmd, dir_name) = line.split_at("$ cd ".len());
                current_idx = *fs_dirs
                    .get(current_idx)
                    .expect("should exist in fs")
                    .dirs
                    .get(dir_name)
                    .expect("dir should exist when cd into");
            } else {
                let mut splits = line.split(" ");
                let size: u32 = splits
                    .next()
                    .expect("should have size")
                    .parse()
                    .expect("size should parse");
                let name: String = splits.next().expect("should have name").to_string();
                fs_dirs
                    .get_mut(current_idx)
                    .expect("should exist in fs")
                    .files
                    .insert(name, size);
            }
        }

        FileSystem {
            total_space,
            directories: fs_dirs,
        }
    }

    fn root(&self) -> Option<&Directory> {
        self.directories.first()
    }

    fn size_of(&self, path: &[&str]) -> u32 {
        let mut current: usize = 0;
        for p in path {
            current = *self.directories.get(current).unwrap().dirs.get(*p).unwrap();
        }

        self.size_of_dir(current)
    }

    fn size_of_dir(&self, idx: usize) -> u32 {
        let dir = self.directories.get(idx).unwrap();
        let file_size: u32 = dir.files_size();
        let dir_size: u32 = dir.dirs.iter().map(|(_name, i)| self.size_of_dir(*i)).sum();

        file_size + dir_size
    }

    fn dir_sizes(&self) -> Vec<u32> {
        (0..self.directories.len())
            .map(|i| self.size_of_dir(i))
            .collect()
    }

    fn optimally_free(&self, space: &u32) -> Option<u32> {
        let dir_sizes = self.dir_sizes();
        let used: u32 = self.size_of_dir(0);
        let free_space: u32 = self.total_space - used;
        if space <= &free_space {
            return None;
        }

        let space_to_free: u32 = space - &free_space;
        dir_sizes.into_iter().filter(|s| s > &space_to_free).min()
    }
}

impl Directory {
    fn empty(parent: Option<usize>) -> Directory {
        Directory {
            parent,
            files: HashMap::new(),
            dirs: HashMap::new(),
        }
    }

    fn files_size(&self) -> u32 {
        self.files.iter().map(|(_, val)| val).sum()
    }

    fn nb_files(&self) -> usize {
        self.files.len()
    }

    fn nb_dirs(&self) -> usize {
        self.dirs.len()
    }
}

fn test() {
    println!("[Day 7] Tests");
    let total_space: u32 = 70000000;

    let fs = read_file("test.txt", total_space);
    let dir = fs.root().expect("should have root");
    println!("{}", dir.nb_files() == 2);
    println!("{}", dir.nb_dirs() == 2);
    println!("");
    println!("{}", fs.size_of(&["a"]) == 94853);
    println!("{}", fs.size_of(&["a", "e"]) == 584);
    println!("{}", fs.size_of(&["d"]) == 24933642);
    println!("{}", fs.size_of(&[]) == 48381165);
    println!("");
    println!(
        "{}",
        fs.dir_sizes()
            .into_iter()
            .filter(|s| s < &100000u32)
            .sum::<u32>()
            == 95437
    );
    println!(
        "{}",
        fs.dir_sizes()
            .into_iter()
            .filter(|s| s < &600u32)
            .sum::<u32>()
            == 584
    );
    println!("{}", fs.optimally_free(&30000000) == Some(24933642));

    println!("");
}

fn read_file(path: &str, total_space: u32) -> FileSystem {
    FileSystem::from_str(
        fs::read_to_string(path).expect("Should be able to load file"),
        total_space,
    )
}

fn solve_part_1(fs: &FileSystem) -> u32 {
    fs.dir_sizes()
        .into_iter()
        .filter(|s| s <= &100000u32)
        .sum::<u32>()
}

fn solve() {
    println!("[Day 7] Solving");
    let total_space: u32 = 70000000;

    let fs = read_file("input.txt", total_space);
    println!("Over 100_000: {}", solve_part_1(&fs));
    println!(
        "Free space: {}",
        fs.optimally_free(&30000000).expect("Should exist")
    );
}

fn main() {
    test();

    solve();
}
