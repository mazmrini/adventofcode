use std::fs;

enum Op {
    Noop,
    AddX(i32),
}

impl Op {
    fn from_str(value: String) -> Vec<Op> {
        value
            .split("\n")
            .map(|el| {
                if el == "noop" {
                    return Op::Noop;
                }

                let mut splits = el.split(" ");
                splits.next().expect("should be addx <n>");

                Op::AddX(
                    splits
                        .next()
                        .expect("should contain the add value")
                        .parse::<i32>()
                        .expect("should parse"),
                )
            })
            .collect::<Vec<Op>>()
    }
}

#[derive(Eq, PartialEq)]
struct Cpu {
    x_register: i32,
}

impl Cpu {
    fn new() -> Cpu {
        Cpu { x_register: 1 }
    }

    fn execute(&mut self, ops: &Vec<Op>) -> Vec<(i32, i32)> {
        let mut history: Vec<(i32, i32)> = vec![(self.x_register, self.x_register)];
        for op in ops.iter() {
            match op {
                Op::Noop => {
                    history.push((self.x_register, self.x_register));
                }
                Op::AddX(i) => {
                    history.push((self.x_register, self.x_register));
                    history.push((self.x_register, self.x_register + i));
                    self.x_register += i;
                }
            }
        }

        history
    }
}

fn compute_signal(history: &Vec<(i32, i32)>, over: &[usize]) -> i32 {
    over.iter()
        .map(|i| (*i as i32) * history.get(*i).unwrap().0)
        .sum::<i32>()
}

fn draw(history: &Vec<(i32, i32)>, width: usize) -> String {
    let pixels: Vec<char> = history
        .get(1..)
        .unwrap()
        .iter()
        .enumerate()
        .map(|(i, h)| {
            let (mid_sprite, _end) = h;
            let i_as_col = (i % width) as i32;

            if (i_as_col < mid_sprite - 1) || (i_as_col > mid_sprite + 1) {
                return ' ';
            }

            '#'
        })
        .collect();

    let mut drawing = String::new();
    for (i, p) in pixels.iter().enumerate() {
        if i % width == 0 {
            drawing.push('\n');
        }
        drawing.push(*p);
    }

    drawing
}

fn test() {
    println!("[Day 10] Tests");

    let mut cpu = Cpu::new();
    let result = cpu.execute(&vec![Op::Noop, Op::AddX(3), Op::AddX(-5)]);
    println!("{}", cpu == Cpu { x_register: -1 });
    println!("{}", result.len() == 6);
    println!("{}", result.get(0) == Some(&(1, 1)));
    println!("{}", result.get(1) == Some(&(1, 1)));
    println!("{}", result.get(2) == Some(&(1, 1)));
    println!("{}", result.get(3) == Some(&(1, 4)));
    println!("{}", result.get(4) == Some(&(4, 4)));
    println!("{}", result.get(5) == Some(&(4, -1)));
    println!("{}", compute_signal(&result, &[1, 3, 5]) == 24);

    let mut cpu = Cpu::new();
    let ops = read_file("test.txt");
    let result = cpu.execute(&ops);
    println!("{}", ops.len() == 146);
    println!("{}", result.get(20).unwrap().0 == 21);
    println!("{}", result.get(60).unwrap().0 == 19);
    println!("{}", result.get(100).unwrap().0 == 18);
    println!("{}", result.get(140).unwrap().0 == 21);
    println!("{}", result.get(180).unwrap().0 == 16);
    println!("{}", result.get(220).unwrap().0 == 18);
    println!(
        "{}",
        compute_signal(&result, &[20, 60, 100, 140, 180, 220]) == 13140
    );

    println!("");
}

fn read_file(path: &str) -> Vec<Op> {
    Op::from_str(fs::read_to_string(path).expect("Should be able to load file"))
}

fn solve() {
    println!("[Day 10] Solving");

    let mut cpu = Cpu::new();
    let ops = read_file("input.txt");
    let history = cpu.execute(&ops);
    println!(
        "Signal {}",
        compute_signal(&history, &[20, 60, 100, 140, 180, 220])
    );

    println!("Drawing:\n{}", draw(&history, 40));
}

fn main() {
    test();

    solve();
}
