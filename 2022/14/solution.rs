use std::cmp;
use std::collections::HashSet;
use std::fs;

struct Cave {
    sand_start: (i16, i16),
    rocks: HashSet<(i16, i16)>,
    floor_y: i16,
}

impl Cave {
    fn from_str(value: &str, sand_start: (i16, i16)) -> Cave {
        let mut rocks: HashSet<(i16, i16)> = HashSet::new();
        for line in value.split("\n") {
            let points: Vec<(i16, i16)> = line
                .split(" -> ")
                .map(|p| {
                    let mut x_y = p.split(",");
                    let x: i16 = x_y.next().unwrap().parse().unwrap();
                    let y: i16 = x_y.next().unwrap().parse().unwrap();

                    (x, y)
                })
                .collect();

            for i in 1..points.len() {
                let prev = points.get(i - 1).unwrap();
                let current = points.get(i).unwrap();
                for p in Cave::points_within(prev, current).into_iter() {
                    rocks.insert(p);
                }
            }
        }
        let floor_y = *rocks.iter().map(|(_x, y)| y).max().unwrap() + 2;

        Cave { rocks, sand_start, floor_y }
    }

    pub fn watch_sand_fall_until_blocks_start(&self) -> Vec<(i16, i16)> {
        let mut rocks = self.rocks.clone();
        let mut sand_rest_spots: Vec<(i16, i16)> = vec![];

        loop {
            let sand = self.fall(&self.sand_start, &rocks);
            sand_rest_spots.push(sand);
            rocks.insert(sand);
            if sand == self.sand_start {
                break;
            }
        }

        sand_rest_spots
    }

    pub fn watch_sand_fall(&self) -> Vec<(i16, i16)> {
        let mut rocks = self.rocks.clone();
        let mut sand_rest_spots: Vec<(i16, i16)> = vec![];

        loop {
            let sand = self.fall(&self.sand_start, &rocks);
            if self.is_on_floor(&sand) {
                break;
            }

            sand_rest_spots.push(sand);
            rocks.insert(sand);
        }

        sand_rest_spots
    }

    pub fn nb_rocks(&self) -> usize {
        self.rocks.len()
    }

    fn is_on_floor(&self, point: &(i16, i16)) -> bool {
        point.1 == self.floor_y - 1
    }

    fn fall(&self, point: &(i16, i16), rocks: &HashSet<(i16, i16)>) -> (i16 ,i16) {
        if self.is_on_floor(point) {
            return *point;
        }

        let one_below = &(point.0, point.1 + 1);
        if !rocks.contains(one_below) {
            return self.fall(one_below, rocks);
        }

        let left_diag = &(point.0 - 1, point.1 + 1);
        if !rocks.contains(left_diag) {
            return self.fall(left_diag, rocks);
        }

        let right_diag = &(point.0 + 1, point.1 + 1);
        if !rocks.contains(right_diag) {
            return self.fall(right_diag, rocks);
        }

        *point
    }

    fn points_within(from: &(i16, i16), to: &(i16, i16)) -> Vec<(i16, i16)> {
        if from.0 == to.0 {
            return (cmp::min(from.1, to.1)..=cmp::max(from.1, to.1))
                .map(|y| (from.0, y))
                .collect();
        }
        if from.1 == to.1 {
            return (cmp::min(from.0, to.0)..=cmp::max(from.0, to.0))
                .map(|x| (x, from.1))
                .collect();
        }

        vec![]
    }
}

fn test() {
    println!("[Day 14] Tests");

    let cave = Cave::from_str(
        "498,4 -> 498,6 -> 496,6\n503,4 -> 502,4 -> 502,9 -> 494,9",
        (500, 0),
    );
    println!("{}", cave.nb_rocks() == 20);

    let sand_spots = cave.watch_sand_fall();
    println!("{}", sand_spots.len() == 24);
    println!("{}", sand_spots.get(0) == Some(&(500, 8)));
    println!("{}", sand_spots.get(1) == Some(&(499, 8)));
    println!("{}", sand_spots.get(2) == Some(&(501, 8)));
    println!("{}", sand_spots.get(3) == Some(&(500, 7)));
    println!("{}", sand_spots.get(4) == Some(&(498, 8)));
    println!("{}", sand_spots.get(5) == Some(&(499, 7)));
    println!("{}", sand_spots.get(6) == Some(&(501, 7)));
    println!("{}", sand_spots.get(22) == Some(&(497, 5)));
    println!("{}", sand_spots.get(23) == Some(&(495, 8)));

    let sand_spots = cave.watch_sand_fall_until_blocks_start();
    println!("{}", sand_spots.len() == 93);

    println!("");
}

fn read_file(path: &str) -> Cave {
    Cave::from_str(&fs::read_to_string(path).expect("Should be able to load file"), (500, 0))
}

fn solve() {
    println!("[Day 14] Solving");

    let cave = read_file("input.txt");

    let spots = cave.watch_sand_fall();
    println!("Units of sand: {}", spots.len());

    let spots = cave.watch_sand_fall_until_blocks_start();
    println!("Blocked after: {}", spots.len());
}

fn main() {
    test();

    solve();
}
