use std::collections::HashMap;
use std::collections::HashSet;
use std::convert::TryInto;
use std::fs;
use std::iter::FromIterator;

fn points_map() -> HashMap<char, u32> {
    let mut result: HashMap<char, u32> = HashMap::new();
    let lower = "abcdefghijklmnopqrstuvwxyz";
    for (i, c) in lower.chars().enumerate() {
        let index = TryInto::<u32>::try_into(i).unwrap();
        result.insert(c, index + 1);
        result.insert(
            *c.to_uppercase().collect::<Vec<char>>().first().unwrap(),
            index + 27,
        );
    }

    result
}

struct Rucksack {
    contents: Vec<String>,
}

impl Rucksack {
    fn new(contents: &[&str]) -> Rucksack {
        Rucksack {
            contents: contents.iter().map(|el| el.to_string()).collect(),
        }
    }

    fn split(contents: &str) -> Rucksack {
        let (left, right) = contents.split_at(contents.len() / 2);
        Rucksack {
            contents: vec![left.to_string(), right.to_string()],
        }
    }

    fn find_common_items(&self) -> HashSet<char> {
        let mut item_sets = self
            .contents
            .iter()
            .map(|content| HashSet::from_iter(content.chars()));

        let mut common: HashSet<char> = item_sets.next().unwrap();
        for set in item_sets {
            common = common
                .intersection(&set)
                .map(|el| *el)
                .collect::<HashSet<char>>();
        }

        common
    }
}

struct Calc {
    points_map: HashMap<char, u32>,
}

impl Calc {
    fn new() -> Calc {
        Calc {
            points_map: points_map(),
        }
    }

    fn compute_points(&self, sacks: &Vec<Rucksack>) -> u32 {
        sacks
            .iter()
            .map(|s| s.find_common_items())
            .map(|items| {
                items
                    .iter()
                    .map(|i| self.points_map.get(i).expect("item not found"))
                    .sum::<u32>()
            })
            .sum::<u32>()
    }
}

fn test() {
    let calc = Calc::new();

    println!(
        "{}",
        calc.compute_points(&vec![Rucksack::split("vJrwpWtwJgWrhcsFMMfFFhFp")]) == 16
    );
    println!(
        "{}",
        calc.compute_points(&vec![Rucksack::split("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL")]) == 38
    );
    println!(
        "{}",
        calc.compute_points(&vec![
            Rucksack::split("vJrwpWtwJgWrhcsFMMfFFhFp"),
            Rucksack::split("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"),
            Rucksack::split("PmmdzqPrVvPwwTWBwg"),
            Rucksack::split("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"),
            Rucksack::split("ttgJtRGJQctTZtZT"),
            Rucksack::split("CrZsJsPPZsGzwwsLwLmpwMDw"),
        ]) == 157
    );
    println!(
        "{}",
        calc.compute_points(&vec![Rucksack::new(&[
            "vJrwpWtwJgWrhcsFMMfFFhFp",
            "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
            "PmmdzqPrVvPwwTWBwg"
        ])]) == 18
    );
    println!(
        "{}",
        calc.compute_points(&vec![Rucksack::new(&[
            "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
            "ttgJtRGJQctTZtZT",
            "CrZsJsPPZsGzwwsLwLmpwMDw"
        ])]) == 52
    );
    println!(
        "{}",
        calc.compute_points(&vec![
            Rucksack::new(&[
                "vJrwpWtwJgWrhcsFMMfFFhFp",
                "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
                "PmmdzqPrVvPwwTWBwg"
            ]),
            Rucksack::new(&[
                "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
                "ttgJtRGJQctTZtZT",
                "CrZsJsPPZsGzwwsLwLmpwMDw"
            ])
        ]) == 70
    );

    println!("");
}

fn read_file_single(path: &str) -> Vec<Rucksack> {
    fs::read_to_string(path)
        .expect("Should be able to load file")
        .split("\n")
        .map(|el| Rucksack::split(el))
        .collect::<Vec<Rucksack>>()
}

fn read_file_group(path: &str) -> Vec<Rucksack> {
    fs::read_to_string(path)
        .expect("Should be able to load file")
        .split("\n")
        .collect::<Vec<&str>>()
        .chunks_exact(3)
        .map(|el| Rucksack::new(el))
        .collect()
}

fn solve() {
    let sacks_as_single = read_file_single("input.txt");
    let sacks_as_group = read_file_group("input.txt");
    let calc = Calc::new();

    println!("Total: {}", calc.compute_points(&sacks_as_single));
    println!("Group: {}", calc.compute_points(&sacks_as_group))
}

fn main() {
    test();

    solve();
}
