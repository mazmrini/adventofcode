use std::cmp;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs;

#[derive(Copy, Clone)]
struct Instruction(char);

impl Instruction {
    fn from_str(value: &str) -> Vec<Instruction> {
        let mut instr: Vec<Instruction> = vec![];
        for el in value.split("\n") {
            let mut splits = el.split(" ");
            let dir = splits
                .next()
                .expect("a direction")
                .chars()
                .next()
                .expect("should have one char");
            let nb_moves: usize = splits
                .next()
                .expect("number of moves")
                .parse()
                .expect("should parse");

            instr.extend_from_slice(&[Instruction(dir)].repeat(nb_moves));
        }

        instr
    }
}

#[derive(Eq, PartialEq, Copy, Clone)]
struct Point {
    x: i16,
    y: i16,
}

impl Point {
    fn origin() -> Point {
        Point { x: 0, y: 0 }
    }

    fn move_by(&mut self, by: &(i16, i16)) {
        self.x += by.0;
        self.y += by.1;
    }

    fn follow(&mut self, other: &Point) {
        let (x, y) = self.distance_to(other);
        let abs_x = x.abs();
        let abs_y = y.abs();
        let chebyshev = cmp::max(abs_x, abs_y);

        if chebyshev > 1 {
            if abs_x > 0 {
                self.x += abs_x / x;
            }
            if abs_y > 0 {
                self.y += abs_y / y;
            }
        }
    }

    fn distance_to(&self, other: &Point) -> (i16, i16) {
        (other.x - self.x, other.y - self.y)
    }

    fn as_str(&self) -> String {
        format!("{},{}", self.x, self.y)
    }
}

struct Grid {
    queue: Vec<Point>,
    moves: HashMap<char, (i16, i16)>,
}

impl Grid {
    fn new(size: usize) -> Grid {
        Grid {
            queue: (0..size + 1).map(|_i| Point::origin()).collect(),
            moves: HashMap::from([('R', (1, 0)), ('L', (-1, 0)), ('U', (0, 1)), ('D', (0, -1))]),
        }
    }

    fn execute(&mut self, instructions: &Vec<Instruction>) -> Vec<Point> {
        let mut tails_path: Vec<Point> = vec![self.queue.last().unwrap().clone()];
        for instruction in instructions.iter() {
            self.queue.first_mut().unwrap().move_by(
                self.moves
                    .get(&instruction.0)
                    .expect("should have instruction"),
            );

            for i in 1..self.queue.len() {
                let previous = self.queue.get(i - 1).unwrap().clone();
                self.queue.get_mut(i).unwrap().follow(&previous);
            }
            tails_path.push(self.queue.last().unwrap().clone());
        }

        tails_path
    }
}

fn count_distinct(points: &Vec<Point>) -> usize {
    let mut set: HashSet<String> = HashSet::new();
    for p in points.iter().map(|p| p.as_str()) {
        set.insert(p);
    }

    set.len()
}

fn test() {
    println!("[Day 9] Tests");

    let mut point = Point { x: 5, y: 5 };
    let start_spot = Point { x: 5, y: 5 };

    println!("[Tests] move_by");
    point.move_by(&(1, 1));
    println!("{}", point == Point { x: 6, y: 6 });
    point.move_by(&(0, 2));
    println!("{}", point == Point { x: 6, y: 8 });
    point.move_by(&(-1, -1));
    println!("{}", point == Point { x: 5, y: 7 });

    println!("[Tests] Not moving");
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 4, y: 4 });
    println!("{}", point == start_spot);
    point.follow(&Point { x: 4, y: 5 });
    println!("{}", point == start_spot);
    point.follow(&Point { x: 4, y: 6 });
    println!("{}", point == start_spot);
    point.follow(&Point { x: 5, y: 4 });
    println!("{}", point == start_spot);
    point.follow(&Point { x: 5, y: 5 });
    println!("{}", point == start_spot);
    point.follow(&Point { x: 5, y: 6 });
    println!("{}", point == start_spot);
    point.follow(&Point { x: 6, y: 4 });
    println!("{}", point == start_spot);
    point.follow(&Point { x: 6, y: 5 });
    println!("{}", point == start_spot);
    point.follow(&Point { x: 6, y: 6 });
    println!("{}", point == start_spot);

    println!("[Tests] Move on x");
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 5, y: 7 });
    println!("{}", point == Point { x: 5, y: 6 });
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 5, y: 3 });
    println!("{}", point == Point { x: 5, y: 4 });

    println!("[Tests] Move on y");
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 7, y: 5 });
    println!("{}", point == Point { x: 6, y: 5 });
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 3, y: 5 });
    println!("{}", point == Point { x: 4, y: 5 });

    println!("[Tests] Move diagonally right");
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 6, y: 7 });
    println!("{}", point == Point { x: 6, y: 6 });
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 6, y: 3 });
    println!("{}", point == Point { x: 6, y: 4 });
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 7, y: 7 });
    println!("{}", point == Point { x: 6, y: 6 });
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 7, y: 3 });
    println!("{}", point == Point { x: 6, y: 4 });

    println!("[Tests] Move diagonally left");
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 4, y: 7 });
    println!("{}", point == Point { x: 4, y: 6 });
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 4, y: 3 });
    println!("{}", point == Point { x: 4, y: 4 });
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 3, y: 7 });
    println!("{}", point == Point { x: 4, y: 6 });
    let mut point = Point { x: 5, y: 5 };
    point.follow(&Point { x: 3, y: 3 });
    println!("{}", point == Point { x: 4, y: 4 });

    println!("[Tests] Grid size 1");
    let instructions = Instruction::from_str("R 4\nU 4\nL 3\nD 1\nR 4\nD 1\nL 5\nR 2");
    let mut grid = Grid::new(1);
    let tail_path = grid.execute(&instructions);
    println!("{}", tail_path.first() == Some(&Point { x: 0, y: 0 }));
    println!("{}", tail_path.get(5) == Some(&Point { x: 3, y: 0 }));
    println!("{}", tail_path.get(10) == Some(&Point { x: 3, y: 4 }));
    println!("{}", tail_path.last() == Some(&Point { x: 1, y: 2 }));
    println!("{}", count_distinct(&tail_path) == 13);
    println!("");

    println!("[Tests] Grid size 3");
    let mut grid = Grid::new(3);
    let tail_path = grid.execute(&instructions);
    println!("{}", tail_path.first() == Some(&Point { x: 0, y: 0 }));
    println!("{}", tail_path.get(6) == Some(&Point { x: 2, y: 1 }));
    println!("{}", tail_path.get(9) == Some(&Point { x: 3, y: 2 }));
    println!("{}", tail_path.get(14) == Some(&Point { x: 3, y: 2 }));
    println!("{}", tail_path.last() == Some(&Point { x: 3, y: 2 }));

    println!("[Tests] Grid size 9");
    let instructions = Instruction::from_str("R 5\nU 8\nL 8\nD 3\nR 17\nD 10\nL 25\nU 20");
    let mut grid = Grid::new(9);
    let tail_path = grid.execute(&instructions);
    println!("{}", tail_path.first() == Some(&Point { x: 0, y: 0 }));
    println!("{}", tail_path.get(3) == Some(&Point { x: 0, y: 0 }));
    println!("{}", tail_path.get(8) == Some(&Point { x: 0, y: 0 }));
    println!("{}", tail_path.get(13) == Some(&Point { x: 0, y: 0 }));
    println!("{}", tail_path.get(21) == Some(&Point { x: 1, y: 3 }));
    println!("{}", tail_path.get(24) == Some(&Point { x: 1, y: 3 }));
    println!("{}", tail_path.get(41) == Some(&Point { x: 5, y: 5 }));
    println!("{}", tail_path.get(51) == Some(&Point { x: 10, y: 0 }));
    println!("{}", tail_path.get(76) == Some(&Point { x: -2, y: -5 }));
    println!("{}", tail_path.last() == Some(&Point { x: -11, y: 6 }));
    println!("{}", count_distinct(&tail_path) == 36);

    println!("");
}

fn read_file(path: &str) -> Vec<Instruction> {
    Instruction::from_str(&fs::read_to_string(path).expect("Should be able to load file"))
}

fn solve() {
    println!("[Day 9] Solving");

    let mut grid = Grid::new(1);
    let instructions = read_file("input.txt");
    println!(
        "Count unique cells: {}",
        count_distinct(&grid.execute(&instructions))
    );

    let mut grid = Grid::new(9);
    println!("Tail: {}", count_distinct(&grid.execute(&instructions)));
}

fn main() {
    test();

    solve();
}
