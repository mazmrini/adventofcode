use std::collections::HashSet;
use std::fs;

struct Signal {
    code: String,
}

impl Signal {
    fn new(code: &str) -> Signal {
        Signal {
            code: code.to_string(),
        }
    }

    fn find_start_marker(&self, unique_count: usize) -> Option<usize> {
        if self.code.len() < unique_count {
            return None;
        }
        for i in 0..self.code.len() - unique_count + 1 {
            let sig: HashSet<char> = self
                .code
                .get(i..i + unique_count)
                .unwrap()
                .chars()
                .collect();
            if sig.len() == unique_count {
                return Some(i + unique_count);
            }
        }

        return None;
    }
}

fn test() {
    println!("[Day 6] Tests");
    let count: usize = 4;
    println!("{}", Signal::new("a").find_start_marker(count).is_none());
    println!("{}", Signal::new("ab").find_start_marker(count).is_none());
    println!("{}", Signal::new("abc").find_start_marker(count).is_none());
    println!(
        "{}",
        Signal::new("aaaaaaaaa").find_start_marker(count).is_none()
    );

    println!(
        "{}",
        Signal::new("abcd").find_start_marker(count).expect("abcd") == 4
    );
    println!(
        "{}",
        Signal::new("mjqjpqmgbljsphdztnvjfqwrcgsmlb")
            .find_start_marker(count)
            .unwrap()
            == 7
    );
    println!(
        "{}",
        Signal::new("bvwbjplbgvbhsrlpgdmjqwftvncz")
            .find_start_marker(count)
            .unwrap()
            == 5
    );
    println!(
        "{}",
        Signal::new("nppdvjthqldpwncqszvftbrmjlhg")
            .find_start_marker(count)
            .unwrap()
            == 6
    );
    println!(
        "{}",
        Signal::new("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")
            .find_start_marker(count)
            .unwrap()
            == 10
    );
    println!(
        "{}",
        Signal::new("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")
            .find_start_marker(count)
            .unwrap()
            == 11
    );

    let count: usize = 14;
    println!(
        "{}",
        Signal::new("mjqjpqmgbljsphdztnvjfqwrcgsmlb")
            .find_start_marker(count)
            .unwrap()
            == 19
    );
    println!(
        "{}",
        Signal::new("bvwbjplbgvbhsrlpgdmjqwftvncz")
            .find_start_marker(count)
            .unwrap()
            == 23
    );
    println!(
        "{}",
        Signal::new("nppdvjthqldpwncqszvftbrmjlhg")
            .find_start_marker(count)
            .unwrap()
            == 23
    );
    println!(
        "{}",
        Signal::new("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")
            .find_start_marker(count)
            .unwrap()
            == 29
    );
    println!(
        "{}",
        Signal::new("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")
            .find_start_marker(count)
            .unwrap()
            == 26
    );

    println!("");
}

fn read_file(path: &str) -> Signal {
    let data = fs::read_to_string(path).expect("Should be able to load file");

    Signal::new(&data)
}

fn solve() {
    println!("[Day 6] Solving");

    let signal = read_file("input.txt");
    println!(
        "Marker @: {}",
        signal.find_start_marker(4).expect("should work")
    );
    println!(
        "Message marker @: {}",
        signal.find_start_marker(14).expect("should work")
    );
}

fn main() {
    test();

    solve();
}
